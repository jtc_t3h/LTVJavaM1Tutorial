import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import vn.t3h.chapter1.FrmBai2_Hello;
import vn.t3h.chapter1.FrmBai3_Tong;
import vn.t3h.chapter1.FrmBai4_HoaDon;

import javax.swing.JLabel;
import javax.swing.ImageIcon;
import java.awt.Color;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class FrmTutorialModule1 extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmTutorialModule1 frame = new FrmTutorialModule1();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmTutorialModule1() {
		setTitle("LTV Java: Bài tập mô đun 1");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 751, 514);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnChng = new JMenu("Chương 1");
		menuBar.add(mnChng);
		
		JMenuItem mntmBi = new JMenuItem("Bài 1 - Xuất Hello trong console");
		mntmBi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
			}
		});
		mnChng.add(mntmBi);
		
		JMenuItem mntmBi_1 = new JMenuItem("Bài 2 - Xuất Hello trong Frame");
		mntmBi_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				FrmBai2_Hello frm = new FrmBai2_Hello();
				frm.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
				frm.setVisible(true);
			}
		});
		mnChng.add(mntmBi_1);
		
		JMenuItem mntmBi_2 = new JMenuItem("Bài 3 - Tính tổng 2 số");
		mntmBi_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				FrmBai3_Tong frm = new FrmBai3_Tong();
				frm.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
				frm.setVisible(true);
			}
		});
		mnChng.add(mntmBi_2);
		
		JMenuItem mntmBi_3 = new JMenuItem("Bài 4 - Hóa đơn");
		mntmBi_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				FrmBai4_HoaDon frm = new FrmBai4_HoaDon();
				frm.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
				frm.setVisible(true);
			}
		});
		mnChng.add(mntmBi_3);
		
		JMenu mnChng_1 = new JMenu("Chương 2");
		menuBar.add(mnChng_1);
		
		JMenu mnChng_2 = new JMenu("Chương 3");
		menuBar.add(mnChng_2);
		
		JMenu mnChng_3 = new JMenu("Chương 4");
		menuBar.add(mnChng_3);
		
		JMenu mnChng_4 = new JMenu("Chương 5");
		menuBar.add(mnChng_4);
		
		JMenu mnChng_5 = new JMenu("Chương 6");
		menuBar.add(mnChng_5);
		
		JMenu mnChng_6 = new JMenu("Chương 7");
		menuBar.add(mnChng_6);
		
		JMenu mnChng_7 = new JMenu("Chương 8");
		menuBar.add(mnChng_7);
		
		JMenu mnChng_8 = new JMenu("Chương 9");
		menuBar.add(mnChng_8);
		
		JMenu mnThngTin = new JMenu("Thông tin");
		menuBar.add(mnThngTin);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(FrmTutorialModule1.class.getResource("/vn/t3h/resources/java_tutorial.jpg")));
		lblNewLabel.setBounds(261, 191, 472, 285);
		contentPane.add(lblNewLabel);
	}
}
