package vn.t3h.chapter1;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class FrmBai3_Tong extends JFrame {

	private JPanel contentPane;
	private JTextField txt1;
	private JTextField txt2;
	private JTextField txtKQ;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai3_Tong frame = new FrmBai3_Tong();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai3_Tong() {
		setTitle("Tính tổng 2 số nguyên");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 341, 214);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblSNguynTh = new JLabel("Số nguyên thứ nhất");
		lblSNguynTh.setBounds(12, 23, 133, 16);
		contentPane.add(lblSNguynTh);
		
		txt1 = new JTextField();
		txt1.setBounds(163, 20, 151, 22);
		contentPane.add(txt1);
		txt1.setColumns(10);
		
		JLabel lblSNguynTh_1 = new JLabel("Số nguyên thứ hai");
		lblSNguynTh_1.setBounds(12, 55, 133, 16);
		contentPane.add(lblSNguynTh_1);
		
		txt2 = new JTextField();
		txt2.setColumns(10);
		txt2.setBounds(163, 52, 151, 22);
		contentPane.add(txt2);
		
		JLabel lblKtQu = new JLabel("Kết quả");
		lblKtQu.setBounds(12, 87, 133, 16);
		contentPane.add(lblKtQu);
		
		txtKQ = new JTextField();
		txtKQ.setColumns(10);
		txtKQ.setBounds(163, 84, 151, 22);
		contentPane.add(txtKQ);
		
		JButton btnSum = new JButton("Tính tổng");
		btnSum.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String strToanHang1 = txt1.getText().trim();
				String strToanHang2 = txt2.getText().trim();
				
				int toanHang1 = Integer.parseInt(strToanHang1);
				int toanHang2 = Integer.parseInt(strToanHang2);
				
				int kq = toanHang1 + toanHang2;
				String strKQ = String.valueOf(kq);
				
				txtKQ.setText(strKQ);
			}
		});
		btnSum.setBounds(106, 136, 97, 25);
		contentPane.add(btnSum);
	}

}
