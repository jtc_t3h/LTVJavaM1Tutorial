package vn.t3h.chapter1;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class FrmTong2So extends JFrame {

	private JPanel contentPane;
	private JTextField txtToanHang1;
	private JTextField txtToanHang2;
	private JTextField txtResult;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmTong2So frame = new FrmTong2So();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmTong2So() {
		setTitle("Tinh tong 2 so nguyen");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 399, 191);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel label = new JLabel("Toan hang 1");
		label.setBounds(20, 11, 87, 14);
		contentPane.add(label);
		
		JLabel lblToanHang = new JLabel("Toan hang 2");
		lblToanHang.setBounds(20, 39, 87, 14);
		contentPane.add(lblToanHang);
		
		txtToanHang1 = new JTextField();
		txtToanHang1.setBounds(107, 8, 251, 20);
		contentPane.add(txtToanHang1);
		txtToanHang1.setColumns(10);
		
		txtToanHang2 = new JTextField();
		txtToanHang2.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent evt) {
				
				if (evt.getKeyCode() == 10){
					System.out.println("aaaaaaaaaaaaaaaaaaaaa");
					btnTinhTong_actionPerformed();
				}
			}
		});
		txtToanHang2.setColumns(10);
		txtToanHang2.setBounds(107, 36, 251, 20);
		contentPane.add(txtToanHang2);
		
		JLabel lblKetQua = new JLabel("Ket qua");
		lblKetQua.setBounds(20, 70, 87, 14);
		contentPane.add(lblKetQua);
		
		txtResult = new JTextField();
		txtResult.setEditable(false);
		txtResult.setColumns(10);
		txtResult.setBounds(107, 67, 251, 20);
		contentPane.add(txtResult);
		
		JButton btnTinhTong = new JButton("Tinh Tong");
		btnTinhTong.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnTinhTong_actionPerformed();
			}
		});
				
		btnTinhTong.setBounds(138, 117, 89, 23);
		contentPane.add(btnTinhTong);
	}

	protected void btnTinhTong_actionPerformed() {

		txtResult.setText(String.valueOf(Integer.parseInt(txtToanHang1.getText().trim()) + Integer.parseInt(txtToanHang2.getText().trim())));	
	}

}
