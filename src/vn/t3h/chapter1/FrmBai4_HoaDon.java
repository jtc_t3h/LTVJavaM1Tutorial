package vn.t3h.chapter1;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class FrmBai4_HoaDon extends JFrame {

	private JPanel contentPane;
	private JTextField txtSoLuong;
	private JTextField txtDonGia;
	private JTextField txtThanhTien;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai4_HoaDon frame = new FrmBai4_HoaDon();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai4_HoaDon() {
		setTitle("Tính giá trị hóa đơn");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 326, 204);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblSLng = new JLabel("Số lượng");
		lblSLng.setBounds(25, 13, 67, 16);
		contentPane.add(lblSLng);
		
		txtSoLuong = new JTextField();
		txtSoLuong.setBounds(104, 10, 145, 22);
		contentPane.add(txtSoLuong);
		txtSoLuong.setColumns(10);
		
		txtDonGia = new JTextField();
		txtDonGia.setColumns(10);
		txtDonGia.setBounds(104, 42, 145, 22);
		contentPane.add(txtDonGia);
		
		JLabel lblnGi = new JLabel("Đơn giá");
		lblnGi.setBounds(25, 45, 67, 16);
		contentPane.add(lblnGi);
		
		txtThanhTien = new JTextField();
		txtThanhTien.setColumns(10);
		txtThanhTien.setBounds(104, 77, 145, 22);
		contentPane.add(txtThanhTien);
		
		JLabel lblThnhTin = new JLabel("Thành tiền");
		lblThnhTin.setBounds(25, 80, 67, 16);
		contentPane.add(lblThnhTin);
		
		JLabel lblVn = new JLabel("VNĐ");
		lblVn.setBounds(261, 45, 40, 16);
		contentPane.add(lblVn);
		
		JLabel label = new JLabel("VNĐ");
		label.setBounds(261, 80, 40, 16);
		contentPane.add(label);
		
		JButton btnTnhTin = new JButton("Tính tiền");
		btnTnhTin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int soLuong = Integer.parseInt(txtSoLuong.getText().trim());
				int donGia = Integer.parseInt(txtDonGia.getText().trim());
				
				txtThanhTien.setText(String.valueOf(soLuong*donGia));
			}
		});
		btnTnhTin.setBounds(102, 125, 97, 25);
		contentPane.add(btnTnhTin);
	}

}
