package vn.t3h.chapter1;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;

public class FrmBai2_Hello extends JFrame {
	private JTextField txtHoTen;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai2_Hello frame = new FrmBai2_Hello();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai2_Hello() {
		setTitle("Xin chào");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 171);
		getContentPane().setLayout(null);
		
		JLabel lblNhpHTn = new JLabel("Nhập họ tên");
		lblNhpHTn.setBounds(63, 13, 87, 16);
		getContentPane().add(lblNhpHTn);
		
		txtHoTen = new JTextField();
		txtHoTen.setBounds(162, 10, 197, 22);
		getContentPane().add(txtHoTen);
		txtHoTen.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(12, 42, 408, 25);
		getContentPane().add(lblNewLabel);
		
		JButton btnNewButton = new JButton("Xuất lời chào");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String hoTen = txtHoTen.getText().trim();
				
				String hello = "Chào bạn " + hoTen + ", chào mừng bạn đến với ngôn ngữ lập trình java.";
				lblNewLabel.setText(hello);
			}
		});
		btnNewButton.setBounds(151, 91, 114, 25);
		getContentPane().add(btnNewButton);
	}
}
