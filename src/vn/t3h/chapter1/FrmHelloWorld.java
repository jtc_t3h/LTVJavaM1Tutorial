package vn.t3h.chapter1;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextPane;

public class FrmHelloWorld extends JFrame {

	private JPanel contentPane;
	private JTextField txtHoTen;
	private JLabel lblOthers;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmHelloWorld frame = new FrmHelloWorld();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmHelloWorld() {
		setTitle("Ung dung dau tien");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnNewButton = new JButton("Hien thi loi choi");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String hoTen = txtHoTen.getText();
				if (hoTen.equals("")){
					JOptionPane.showMessageDialog(null, "Phai nhap ho ten truoc khi bam Hien Thi");
				}
				
				JOptionPane.showMessageDialog(null, "Welcome to " + hoTen + ", Java SE Basic.");
			}
		});
		btnNewButton.setToolTipText("Ban dang su dung tooltip");
		btnNewButton.setBounds(176, 88, 134, 23);
		contentPane.add(btnNewButton);
		
		txtHoTen = new JTextField();
		txtHoTen.setBounds(99, 57, 325, 20);
		contentPane.add(txtHoTen);
		txtHoTen.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Ho va Ten");
		lblNewLabel.setBounds(27, 60, 68, 14);
		contentPane.add(lblNewLabel);
		
		JButton btnOthersButton = new JButton("Others Button");
		btnOthersButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblOthers.setText("clicked Others Button.");
			}
		});
		btnOthersButton.setBounds(26, 121, 122, 23);
		contentPane.add(btnOthersButton);
		
		lblOthers = new JLabel("New label");
		lblOthers.setBounds(25, 164, 376, 14);
		contentPane.add(lblOthers);
	}
}
