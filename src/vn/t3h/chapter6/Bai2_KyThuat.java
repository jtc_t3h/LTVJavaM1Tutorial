package vn.t3h.chapter6;

public class Bai2_KyThuat extends Bai2_NhanVien {
	private int soDuAn;

	public Bai2_KyThuat(double hesoLuong, int soDuAn) {
		super(hesoLuong);
		this.soDuAn = soDuAn;
	}

	@Override
	public double tinhLuong() {
		return super.tinhLuong() + soDuAn*1000000;
	}
	
	
}
