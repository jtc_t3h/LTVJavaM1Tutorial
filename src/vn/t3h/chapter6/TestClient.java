package vn.t3h.chapter6;

public class TestClient {

	static int count = 0;
	
	
	
	// static block
	static {
		// goi bien count duoc khong? --> bien phai static
		count++;
		System.out.println("Static block");
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
//		Bai1_Animal animal = new Bai1_Animal(); // animail -> animal
//		animal.sayHello();
		
//		System.out.println("animal.toString()=" + animal.toString());
//		System.out.println("animal.hashCode()=" + animal.hashCode());
		
		Bai1_Cat cat = new Bai1_Cat();
		cat.sayHello();
		
//		animal = new Bai1_Cat(); // animal -> Cat
//		System.out.println(animal.sayHello());
//		 
//		animal = new Bai1_Dog(); // animal -> Dog
//		System.out.println(animal.sayHello());
//		
//		animal = new Bai1_Pig(); // animal -> Pig
//		System.out.println(animal.sayHello());
		
//		Bai1_Animal animal1 = new Bai1_Animal();
//		Bai1_Animal animal2 = new Bai1_Animal();
//		
//		System.out.println(animal1.total); // 0 --> Goi thong qua doi tuong
//		animal1.total = 10;
//		System.out.println(Bai1_Animal.total); // 10 -> goi thong qua ten Lop
		
//		Bai3_Shape shape = new Bai3_Circle();
//		
//		MyInt in = new Bai3_Circle();
//		

//		
//		Child child = new Child();
		
		System.out.println(StaticDemo.staVariable); // 0
		
		StaticDemo s1 = new StaticDemo();
		System.out.println(s1.staVariable); // 0
		s1.staVariable = 10;
		
		StaticDemo s2 = new StaticDemo();
		System.out.println(s2.staVariable); // 10
		
		System.out.println(StaticDemo.staVariable); // 10
		
//		System.out.println("Done!");
		
		TestClient t = new TestClient();
		t.abc();
	}
	
	public void abc() {
		System.out.println("Hello VietNam chien thang.");
	}

}
