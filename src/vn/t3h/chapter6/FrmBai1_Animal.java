package vn.t3h.chapter6;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class FrmBai1_Animal extends JFrame {

	private JPanel contentPane;
	private JTextField txtName;
	private JComboBox cbbAnimal;
	private JLabel lblHello;
	private JLabel lblImage;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai1_Animal frame = new FrmBai1_Animal();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai1_Animal() {
		setTitle("Animals say hello");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 459, 245);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblName = new JLabel("Name");
		lblName.setBounds(10, 11, 86, 14);
		contentPane.add(lblName);
		
		txtName = new JTextField();
		txtName.setBounds(106, 8, 127, 20);
		contentPane.add(txtName);
		txtName.setColumns(10);
		
		JLabel lblAnimals = new JLabel("Animal");
		lblAnimals.setBounds(10, 36, 46, 14);
		contentPane.add(lblAnimals);
		
		cbbAnimal = new JComboBox();
		cbbAnimal.setModel(new DefaultComboBoxModel(new String[] {"--- Chọn ---", "Dog", "Cat", "Pig"}));
		cbbAnimal.setBounds(106, 32, 127, 22);
		contentPane.add(cbbAnimal);
		
		JButton btnSayHello = new JButton("Say Hello");
		btnSayHello.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String name = txtName.getText();
				
				switch (cbbAnimal.getSelectedItem().toString()) {
				case "Dog":
					Bai1_Dog d = new Bai1_Dog(name, "/vn/t3h/resources/dog.jpg");
					lblHello.setText(d.getName() + " says " + d.sayHello());
					lblImage.setIcon(new ImageIcon(FrmBai1_Animal.class.getResource(d.getImage())));
					break;
				case "Cat":
					Bai1_Cat c = new Bai1_Cat(name, "/vn/t3h/resources/meo.png");
					lblHello.setText(c.getName() + " says " + c.sayHello());
					lblImage.setIcon(new ImageIcon(FrmBai1_Animal.class.getResource(c.getImage())));
					break;
				case "Pig":
					Bai1_Pig p = new Bai1_Pig(name, "/vn/t3h/resources/pig.jpg");
					lblHello.setText(p.getName() + " says " + p.sayHello());
					lblImage.setIcon(new ImageIcon(FrmBai1_Animal.class.getResource(p.getImage())));
					break;
				}

			}
		});
		btnSayHello.setBounds(103, 65, 91, 23);
		contentPane.add(btnSayHello);
		
		lblHello = new JLabel("New label");
		lblHello.setBounds(10, 111, 223, 14);
		contentPane.add(lblHello);
		
		lblImage = new JLabel("");
		lblImage.setIcon(new ImageIcon(FrmBai1_Animal.class.getResource("/vn/t3h/resources/meo.png")));
		lblImage.setBounds(243, 11, 190, 166);
		contentPane.add(lblImage);
	}
}
