package vn.t3h.chapter6;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.border.EtchedBorder;
import java.awt.Color;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Bai6_FrmPhuongTienGiaoThong extends JFrame {

	private JPanel contentPane;
	private JTextField txtQuangDuong;
	private JTextField txtThoiGian;
	private JTextField txtNhieuLieu;
	private ButtonGroup bgLoaiPhuongTienGiaoThong = new ButtonGroup();
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTextField textField_6;
	private JTextField txtVanTocNuocXuoiDong;
	private JTextField txtVanTocNuocNguocDong;
	private JTextField txtNhienLieuTieuHao;
	private JTextField txtVanToc;
	private JTextField txtTieuThuNhienLieu;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Bai6_FrmPhuongTienGiaoThong frame = new Bai6_FrmPhuongTienGiaoThong();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Bai6_FrmPhuongTienGiaoThong() {
		setTitle("Ph\u01B0\u01A1ng ti\u1EC7n giao th\u00F4ng - \u0111\u1ED9ng c\u01A1");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 689, 427);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "Th\u00F4ng tin chung", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.setBounds(10, 11, 659, 58);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblQungngkm = new JLabel("Qu\u00E3ng \u0111\u01B0\u1EDDng (km)");
		lblQungngkm.setBounds(10, 29, 110, 14);
		panel.add(lblQungngkm);
		
		txtQuangDuong = new JTextField();
		txtQuangDuong.setBounds(130, 26, 86, 20);
		panel.add(txtQuangDuong);
		txtQuangDuong.setColumns(10);
		
		JLabel lblThiGiangi = new JLabel("Th\u1EDDi gian (gi\u1EDD)");
		lblThiGiangi.setBounds(233, 26, 90, 14);
		panel.add(lblThiGiangi);
		
		txtThoiGian = new JTextField();
		txtThoiGian.setColumns(10);
		txtThoiGian.setBounds(353, 23, 86, 20);
		panel.add(txtThoiGian);
		
		JLabel lblNhinLiult = new JLabel("Nhi\u00EAn li\u1EC7u (l\u00EDt)");
		lblNhinLiult.setBounds(463, 26, 90, 14);
		panel.add(lblNhinLiult);
		
		txtNhieuLieu = new JTextField();
		txtNhieuLieu.setColumns(10);
		txtNhieuLieu.setBounds(563, 23, 86, 20);
		panel.add(txtNhieuLieu);
		
		JRadioButton rdbXeHoi = new JRadioButton("Xe H\u01A1i");
		rdbXeHoi.setSelected(true);
		rdbXeHoi.setBounds(208, 88, 109, 23);
		contentPane.add(rdbXeHoi);
		bgLoaiPhuongTienGiaoThong.add(rdbXeHoi);
		
		JRadioButton rdbTauThuy = new JRadioButton("T\u00E0u Th\u1EE7y");
		rdbTauThuy.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				txtVanTocNuocXuoiDong.setEditable(true);
				txtVanTocNuocNguocDong.setEditable(true);
				txtNhienLieuTieuHao.setEditable(true);
				
				// Xe Hoi -> setEditable(false)
				
			}
		});
		rdbTauThuy.setBounds(352, 88, 109, 23);
		contentPane.add(rdbTauThuy);
		bgLoaiPhuongTienGiaoThong.add(rdbTauThuy);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(null, "Xe H\u01A1i", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_1.setBounds(10, 111, 276, 173);
		contentPane.add(panel_1);
		panel_1.setLayout(null);
		
		JLabel lblTnXe = new JLabel("T\u00EAn xe");
		lblTnXe.setBounds(10, 30, 71, 14);
		panel_1.add(lblTnXe);
		
		textField_3 = new JTextField();
		textField_3.setBounds(91, 27, 175, 20);
		panel_1.add(textField_3);
		textField_3.setColumns(10);
		
		JLabel lblHngSnXut = new JLabel("H\u00E3ng s\u1EA3n xu\u1EA5t");
		lblHngSnXut.setBounds(10, 58, 71, 14);
		panel_1.add(lblHngSnXut);
		
		textField_4 = new JTextField();
		textField_4.setColumns(10);
		textField_4.setBounds(91, 55, 175, 20);
		panel_1.add(textField_4);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "T\u00E0u Th\u1EE7y", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel_2.setBounds(296, 111, 373, 173);
		contentPane.add(panel_2);
		panel_2.setLayout(null);
		
		JLabel lblTrngLng = new JLabel("Tr\u1ECDng l\u01B0\u1EE3ng (kg)");
		lblTrngLng.setBounds(10, 28, 168, 14);
		panel_2.add(lblTrngLng);
		
		textField_5 = new JTextField();
		textField_5.setEditable(false);
		textField_5.setColumns(10);
		textField_5.setBounds(188, 25, 175, 20);
		panel_2.add(textField_5);
		
		JLabel lblScTikg = new JLabel("S\u1EE9c t\u1EA3i (kg)");
		lblScTikg.setBounds(10, 56, 168, 14);
		panel_2.add(lblScTikg);
		
		textField_6 = new JTextField();
		textField_6.setEditable(false);
		textField_6.setColumns(10);
		textField_6.setBounds(188, 53, 175, 20);
		panel_2.add(textField_6);
		
		JLabel lblVnTcNc = new JLabel("V\u1EADn t\u1ED1c n\u01B0\u1EDBc xu\u00F4i d\u00F2ng");
		lblVnTcNc.setBounds(10, 84, 168, 14);
		panel_2.add(lblVnTcNc);
		
		txtVanTocNuocXuoiDong = new JTextField();
		txtVanTocNuocXuoiDong.setEditable(false);
		txtVanTocNuocXuoiDong.setColumns(10);
		txtVanTocNuocXuoiDong.setBounds(188, 81, 175, 20);
		panel_2.add(txtVanTocNuocXuoiDong);
		
		JLabel lblVnTcNc_1 = new JLabel("V\u1EADn t\u1ED1c n\u01B0\u1EDBc ng\u01B0\u1EE3c d\u00F2ng");
		lblVnTcNc_1.setBounds(10, 112, 168, 14);
		panel_2.add(lblVnTcNc_1);
		
		txtVanTocNuocNguocDong = new JTextField();
		txtVanTocNuocNguocDong.setEditable(false);
		txtVanTocNuocNguocDong.setColumns(10);
		txtVanTocNuocNguocDong.setBounds(188, 109, 175, 20);
		panel_2.add(txtVanTocNuocNguocDong);
		
		JLabel lblNhie = new JLabel("Nhi\u00EAn li\u1EC7u ti\u00EAu hao");
		lblNhie.setBounds(10, 140, 168, 14);
		panel_2.add(lblNhie);
		
		txtNhienLieuTieuHao = new JTextField();
		txtNhienLieuTieuHao.setEditable(false);
		txtNhienLieuTieuHao.setColumns(10);
		txtNhienLieuTieuHao.setBounds(188, 137, 175, 20);
		panel_2.add(txtNhienLieuTieuHao);
		
		JButton btnNewButton = new JButton("T\u00EDnh v\u1EADn t\u1ED1c - ti\u00EAu th\u1EE5 nhi\u00EAn li\u1EC7u");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				double quangDuong = Double.parseDouble(txtQuangDuong.getText().trim());
				double thoiGian = Double.parseDouble(txtThoiGian.getText().trim());
				double soLit = Double.parseDouble(txtNhieuLieu.getText().trim());
				
				double vanToc = 0.0;
				double tieuThuNhienLieu = 0.0;
				if (rdbXeHoi.isSelected()) {
					Bai6_XeHoi xeHoi = new Bai6_XeHoi();
					vanToc = xeHoi.tinhVanToc(quangDuong, thoiGian);
					tieuThuNhienLieu = xeHoi.tieuThuNhienLieu(quangDuong, soLit);
				} else {
					Bai6_TauThuy tauThuy = new Bai6_TauThuy();
					tauThuy.setVanTocNuocXuoiDong(Double.parseDouble(txtVanTocNuocXuoiDong.getText().trim()));
					tauThuy.setVanTocNuocNguocDong(Double.parseDouble(txtVanTocNuocNguocDong.getText().trim()));
					tauThuy.setNhienLieuTieuHao(Double.parseDouble(txtNhienLieuTieuHao.getText().trim()));
					
					vanToc = tauThuy.tinhVanToc(quangDuong, thoiGian);
					tieuThuNhienLieu = tauThuy.tieuThuNhienLieu(quangDuong, soLit);
				}
				
				txtVanToc.setText(String.valueOf(vanToc));
				txtTieuThuNhienLieu.setText(String.valueOf(tieuThuNhienLieu));
			}
		});
		btnNewButton.setBounds(183, 304, 312, 23);
		contentPane.add(btnNewButton);
		
		JLabel lblVnTckmh = new JLabel("V\u1EADn t\u1ED1c (km/h)");
		lblVnTckmh.setBounds(10, 354, 110, 14);
		contentPane.add(lblVnTckmh);
		
		txtVanToc = new JTextField();
		txtVanToc.setColumns(10);
		txtVanToc.setBounds(130, 351, 86, 20);
		contentPane.add(txtVanToc);
		
		JLabel lblTiuThNhin = new JLabel("Ti\u00EAu th\u1EE5 nhi\u00EAn li\u1EC7u (km/lit)");
		lblTiuThNhin.setBounds(381, 351, 151, 14);
		contentPane.add(lblTiuThNhin);
		
		txtTieuThuNhienLieu = new JTextField();
		txtTieuThuNhienLieu.setColumns(10);
		txtTieuThuNhienLieu.setBounds(560, 348, 86, 20);
		contentPane.add(txtTieuThuNhienLieu);
	}
}
