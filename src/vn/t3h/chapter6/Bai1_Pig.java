package vn.t3h.chapter6;

public class Bai1_Pig extends Bai1_Animal {
	@Override
	public String sayHello() {
		return "Oil oil oil";
	}

	public Bai1_Pig() {
	}

	public Bai1_Pig(String name, String image) {
		super(name, image);
	}
}
