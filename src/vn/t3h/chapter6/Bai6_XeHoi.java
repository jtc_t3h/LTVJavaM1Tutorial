package vn.t3h.chapter6;

public class Bai6_XeHoi implements Bai6_DongCo, Bai6_PhuongTienGiaoThong {
	
	private String tenXe;
	private String hangSanXuat;
	
	

	public Bai6_XeHoi() {
		this.tenXe = null;
		this.hangSanXuat = null;
	}

	public Bai6_XeHoi(String tenXe, String hangSanXuat) {
		this.tenXe = tenXe;
		this.hangSanXuat = hangSanXuat;
	}
	
	
	public String getTenXe() {
		return tenXe;
	}

	public void setTenXe(String tenXe) {
		this.tenXe = tenXe;
	}

	public String getHangSanXuat() {
		return hangSanXuat;
	}

	public void setHangSanXuat(String hangSanXuat) {
		this.hangSanXuat = hangSanXuat;
	}

	@Override
	public double tinhVanToc(double quangDuong, double thoiGian) {
		return quangDuong/thoiGian;
	}

	@Override
	public double tieuThuNhienLieu(double quangDuong, double soLit) {
		return quangDuong/soLit;
	}

}
