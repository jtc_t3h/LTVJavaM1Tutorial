package vn.t3h.chapter6;

public class Bai3_Circle extends Bai3_Shape implements MyInt {
	private double r;
	
	public Bai3_Circle() {
	}

	public Bai3_Circle(double r) {
		this.r = r;
	}

	public void setR(double r) {
		this.r = r;
	}

	public double getR() {
		return r;
	}

	@Override
	public double perimeter() {
		return 2 * Math.PI * r;
	}

	@Override
	public double area() {
		return Math.PI * Math.pow(r, 2);
	}

	@Override
	public void abc() {
		System.out.println("hello ABC");
		
	}

}
