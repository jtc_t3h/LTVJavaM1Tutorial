package vn.t3h.chapter6;

public class Bai1_Cat extends Bai1_Animal {


	public String sayHello() {
		return "Meo meo meo";
	}

	public Bai1_Cat() {
		super();
	}

	public Bai1_Cat(String name, String image) {
		super(name, image);
	}
}
