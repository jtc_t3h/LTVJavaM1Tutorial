package vn.t3h.chapter6;

public class Child extends Parent{

	public Child() {
		super();
	}
	
	public Child(String s) {
		super(s);
	}
	
	@Override // Annotation
	public Double aMethod() {
		// TODO Auto-generated method stub
		return 0.0;
	}

	public Double aMethod(double a) {
		// TODO Auto-generated method stub
		return 0.0;
	}
	
	public Double aMethod(double a, String b) {
		// TODO Auto-generated method stub
		return 0.0;
	}

	public Double aMethod(int a) {
		// TODO Auto-generated method stub
		return 0.0;
	}
	
	public void abc() {
		super.displayParent();
	}
}
