package vn.t3h.chapter6;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.FlowLayout;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class FrmBai2_TinhLuong extends JFrame {

	private JPanel contentPane;
	private JTextField txt1;
	private JTextField txt2;
	private JTextField txtLuong;
	private JComboBox cbbLoaiNV;
	private JLabel lbl2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai2_TinhLuong frame = new FrmBai2_TinhLuong();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai2_TinhLuong() {
		setTitle("T\u00EDnh l\u01B0\u01A1ng nh\u00E2n vi\u00EAn");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 361, 194);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblLoiNv = new JLabel("Lo\u1EA1i NV");
		lblLoiNv.setBounds(10, 11, 120, 14);
		contentPane.add(lblLoiNv);

		cbbLoaiNV = new JComboBox();
		cbbLoaiNV.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				switch (cbbLoaiNV.getSelectedIndex()) {
				case 1: // Hanh chinh
					lbl2.setVisible(true);
					txt2.setVisible(true);
					lbl2.setText("Phụ cấp");
					break;
				case 2: // Ky thuat
					lbl2.setVisible(true);
					txt2.setVisible(true);
					lbl2.setText("Số dự án");
					break;
				case 3: // Kinh doanh
					lbl2.setVisible(true);
					txt2.setVisible(true);
					lbl2.setText("Số sản phẩm");
					break;
				case 4: // Lanh dao
					lbl2.setVisible(false);
					txt2.setVisible(false);
					break;
				default:
					lbl2.setVisible(false);
					txt2.setVisible(false);
					break;
				}
			}
		});
		cbbLoaiNV.setModel(new DefaultComboBoxModel(
				new String[] { "--- Chọn ---", "Hành chính", "Kỹ thuật", "Kinh doanh", "Lãnh đạo" }));
		cbbLoaiNV.setBounds(140, 7, 191, 22);
		contentPane.add(cbbLoaiNV);

		JLabel lbl1 = new JLabel("H\u1EC7 s\u1ED1 l\u01B0\u01A1ng");
		lbl1.setBounds(10, 36, 120, 14);
		contentPane.add(lbl1);

		txt1 = new JTextField();
		txt1.setBounds(139, 33, 192, 20);
		contentPane.add(txt1);
		txt1.setColumns(10);

		lbl2 = new JLabel("H\u1EC7 s\u1ED1 l\u01B0\u01A1ng");
		lbl2.setBounds(10, 64, 120, 14);
		contentPane.add(lbl2);

		txt2 = new JTextField();
		txt2.setColumns(10);
		txt2.setBounds(139, 61, 192, 20);
		contentPane.add(txt2);

		JButton btnTinhLuong = new JButton("T\u00EDnh L\u01B0\u01A1ng");
		btnTinhLuong.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				double hesoLuong = Double.parseDouble(txt1.getText().trim());
				int thongSo = Integer.parseInt(txt2.getText().trim());

				switch (cbbLoaiNV.getSelectedIndex()) {
				case 1: // hanh chinh
					Bai2_HanhChinh hc = new Bai2_HanhChinh(thongSo, hesoLuong);
					txtLuong.setText(String.valueOf(hc.tinhLuong()));
					break;
				case 2: // ky thuat
					Bai2_KyThuat kt = new Bai2_KyThuat(hesoLuong, thongSo);
					txtLuong.setText(String.valueOf(kt.tinhLuong()));
					break;
				case 3: // kinh doanh
					Bai2_KinhDoanh kd = new Bai2_KinhDoanh(hesoLuong, thongSo);
					txtLuong.setText(String.valueOf(kd.tinhLuong()));
					break;
				case 4: // lanh dao
					Bai2_LanhDao ld = new Bai2_LanhDao(hesoLuong);
					txtLuong.setText(String.valueOf(ld.tinhLuong()));
					break;
				}

			}
		});
		btnTinhLuong.setBounds(138, 92, 104, 23);
		contentPane.add(btnTinhLuong);

		JLabel lblLng = new JLabel("L\u01B0\u01A1ng");
		lblLng.setBounds(12, 130, 120, 14);
		contentPane.add(lblLng);

		txtLuong = new JTextField();
		txtLuong.setColumns(10);
		txtLuong.setBounds(141, 127, 192, 20);
		contentPane.add(txtLuong);
	}

}
