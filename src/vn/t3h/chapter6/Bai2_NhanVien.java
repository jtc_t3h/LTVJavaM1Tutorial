package vn.t3h.chapter6;

public class Bai2_NhanVien {
	private final double LUONGCOBAN = 1550000;
	private double hesoLuong;

	public Bai2_NhanVien(double hesoLuong) {
		this.hesoLuong = hesoLuong;
	}

	public double getHesoLuong() {
		return hesoLuong;
	}

	public void setHesoLuong(double hesoLuong) {
		this.hesoLuong = hesoLuong;
	}

	public double tinhLuong() {
		return this.LUONGCOBAN * this.hesoLuong;
	}
}
