package vn.t3h.chapter6;

public class Bai1_Animal {
	private String name;  // bien thuc the = instance variable
	private String image;
	
	public static int total = 0; // static variable

	public String getName() {
		return name;
	}

	public String getImage() {
		return image;
	}

	public Bai1_Animal() {
		System.out.println("Animal: ham khoi tao mac dinh tuong minh!");
	}

	public Bai1_Animal(String name, String _image) { // name, image -> bien cuc bo (local variable)
		this.name = name;
		image = _image;
	}

	public String sayHello() {
		return "Hello!";
	}
	
	public String sayHello(String name) {
		return "Hello!" + name;
	}
	
	public String sayHello(int id) {
		return "Hello!" + id;
	}
	
	public void sayHello(String name, int id) {
		
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return name;
	}

	public static int getTotal() {
		return total;
	}

	public static void setTotal(int total) {
		Bai1_Animal.total = total;
	}
	
	
	
}
