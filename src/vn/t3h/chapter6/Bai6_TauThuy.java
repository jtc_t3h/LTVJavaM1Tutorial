package vn.t3h.chapter6;

public class Bai6_TauThuy implements Bai6_DongCo, Bai6_PhuongTienGiaoThong{

	private long trongLuong;
	private long sucTai;
	private double vanTocNuocXuoiDong;
	private double vanTocNuocNguocDong;
	private double nhienLieuTieuHao;
	
	public Bai6_TauThuy() {
	}

	public long getTrongLuong() {
		return trongLuong;
	}

	public void setTrongLuong(long trongLuong) {
		this.trongLuong = trongLuong;
	}

	public long getSucTai() {
		return sucTai;
	}

	public void setSucTai(long sucTai) {
		this.sucTai = sucTai;
	}

	public double getVanTocNuocXuoiDong() {
		return vanTocNuocXuoiDong;
	}

	public void setVanTocNuocXuoiDong(double vanTocNuocXuoiDong) {
		this.vanTocNuocXuoiDong = vanTocNuocXuoiDong;
	}

	public double getVanTocNuocNguocDong() {
		return vanTocNuocNguocDong;
	}

	public void setVanTocNuocNguocDong(double vanTocNuocNguocDong) {
		this.vanTocNuocNguocDong = vanTocNuocNguocDong;
	}

	public double getNhienLieuTieuHao() {
		return nhienLieuTieuHao;
	}

	public void setNhienLieuTieuHao(double nhienLieuTieuHao) {
		this.nhienLieuTieuHao = nhienLieuTieuHao;
	}

	@Override
	public double tinhVanToc(double quangDuong, double thoiGian) {
		return quangDuong/thoiGian + vanTocNuocXuoiDong - vanTocNuocNguocDong;
	}

	@Override
	public double tieuThuNhienLieu(double quangDuong, double soLit) {
		return quangDuong/(soLit - nhienLieuTieuHao);
	}
	
	
	
	
}
