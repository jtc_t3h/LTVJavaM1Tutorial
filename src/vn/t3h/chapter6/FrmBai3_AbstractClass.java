package vn.t3h.chapter6;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTabbedPane;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class FrmBai3_AbstractClass extends JFrame {

	private JPanel contentPane;
	private JTextField txtBanKinh;
	private JTextField txtCVHinhTron;
	private JTextField txtDTHinhTron;
	private JTextField txtChieuDai;
	private JTextField txtChieuRong;
	private JTextField txtCVHinhChuNhat;
	private JTextField txtDTHinhChuNhat;
	private JTextField txtCanh;
	private JTextField txtCVHinhVuong;
	private JTextField txtDTHinhVuong;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai3_AbstractClass frame = new FrmBai3_AbstractClass();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai3_AbstractClass() {
		setTitle("Abstract class");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 232);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(0, 0, 434, 261);
		contentPane.add(tabbedPane);

		JPanel panel = new JPanel();
		tabbedPane.addTab("Hình tròn", null, panel, null);
		panel.setLayout(null);

		JLabel lblChiuDi = new JLabel("Bán kính");
		lblChiuDi.setBounds(10, 11, 75, 14);
		panel.add(lblChiuDi);

		txtBanKinh = new JTextField();
		txtBanKinh.setBounds(95, 8, 205, 20);
		panel.add(txtBanKinh);
		txtBanKinh.setColumns(10);

		JLabel lblChuVi = new JLabel("Chu vi");
		lblChuVi.setBounds(10, 39, 75, 14);
		panel.add(lblChuVi);

		txtCVHinhTron = new JTextField();
		txtCVHinhTron.setColumns(10);
		txtCVHinhTron.setBounds(95, 36, 205, 20);
		panel.add(txtCVHinhTron);

		JLabel lblDinTch = new JLabel("Diện tích");
		lblDinTch.setBounds(10, 67, 75, 14);
		panel.add(lblDinTch);

		txtDTHinhTron = new JTextField();
		txtDTHinhTron.setColumns(10);
		txtDTHinhTron.setBounds(95, 64, 205, 20);
		panel.add(txtDTHinhTron);

		JButton btnCVHinhTron = new JButton("Tính CV");
		btnCVHinhTron.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				double banKinh = Double.parseDouble(txtBanKinh.getText().trim());

				Bai3_Circle c = new Bai3_Circle(banKinh);
				txtCVHinhTron.setText(String.valueOf(c.perimeter()));
			}
		});
		btnCVHinhTron.setBounds(313, 7, 89, 23);
		panel.add(btnCVHinhTron);

		JButton btnTinhDTHinhTron = new JButton("Tính DT");
		btnTinhDTHinhTron.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				double banKinh = Double.parseDouble(txtBanKinh.getText().trim());

				Bai3_Circle c = new Bai3_Circle(banKinh);
				txtDTHinhTron.setText(String.valueOf(c.area()));
			}
		});
		btnTinhDTHinhTron.setBounds(313, 35, 89, 23);
		panel.add(btnTinhDTHinhTron);

		JPanel panel_1 = new JPanel();
		tabbedPane.addTab("Hình chữ nhật", null, panel_1, null);
		panel_1.setLayout(null);
		
		JLabel lblHnhChNht = new JLabel("Chiều dài");
		lblHnhChNht.setBounds(10, 11, 73, 14);
		panel_1.add(lblHnhChNht);
		
		txtChieuDai = new JTextField();
		txtChieuDai.setBounds(93, 8, 227, 20);
		panel_1.add(txtChieuDai);
		txtChieuDai.setColumns(10);
		
		JLabel label = new JLabel("Chiều dài");
		label.setBounds(10, 39, 73, 14);
		panel_1.add(label);
		
		txtChieuRong = new JTextField();
		txtChieuRong.setColumns(10);
		txtChieuRong.setBounds(93, 36, 227, 20);
		panel_1.add(txtChieuRong);
		
		JLabel label_1 = new JLabel("Chiều dài");
		label_1.setBounds(10, 67, 73, 14);
		panel_1.add(label_1);
		
		txtCVHinhChuNhat = new JTextField();
		txtCVHinhChuNhat.setColumns(10);
		txtCVHinhChuNhat.setBounds(93, 64, 227, 20);
		panel_1.add(txtCVHinhChuNhat);
		
		JLabel label_2 = new JLabel("Chiều dài");
		label_2.setBounds(10, 95, 73, 14);
		panel_1.add(label_2);
		
		txtDTHinhChuNhat = new JTextField();
		txtDTHinhChuNhat.setColumns(10);
		txtDTHinhChuNhat.setBounds(93, 92, 227, 20);
		panel_1.add(txtDTHinhChuNhat);
		
		JButton btnCVHinhChuNhat = new JButton("Tính CV");
		btnCVHinhChuNhat.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				double chieuDai = Double.parseDouble(txtChieuDai.getText().trim());
				double chieuRong = Double.parseDouble(txtChieuRong.getText().trim());
				
				Bai3_Rectangle r = new Bai3_Rectangle(chieuDai, chieuRong);
				txtCVHinhChuNhat.setText(String.valueOf(r.perimeter()));
			}
		});
		btnCVHinhChuNhat.setBounds(330, 7, 89, 23);
		panel_1.add(btnCVHinhChuNhat);
		
		JButton btnDTHinhChuNhat = new JButton("Tính DT");
		btnDTHinhChuNhat.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				double chieuDai = Double.parseDouble(txtChieuDai.getText().trim());
				double chieuRong = Double.parseDouble(txtChieuRong.getText().trim());
				
				Bai3_Rectangle r = new Bai3_Rectangle(chieuDai, chieuRong);
				txtDTHinhChuNhat.setText(String.valueOf(r.area()));
			}
		});
		btnDTHinhChuNhat.setBounds(330, 35, 89, 23);
		panel_1.add(btnDTHinhChuNhat);

		JPanel panel_2 = new JPanel();
		tabbedPane.addTab("Hình vuông", null, panel_2, null);
		panel_2.setLayout(null);
		
		JLabel lblChiuDiCnh = new JLabel("Chiều dài cạnh");
		lblChiuDiCnh.setBounds(10, 11, 93, 14);
		panel_2.add(lblChiuDiCnh);
		
		txtCanh = new JTextField();
		txtCanh.setBounds(113, 8, 196, 20);
		panel_2.add(txtCanh);
		txtCanh.setColumns(10);
		
		JLabel lblChuVi_1 = new JLabel("Chu vi");
		lblChuVi_1.setBounds(10, 39, 93, 14);
		panel_2.add(lblChuVi_1);
		
		txtCVHinhVuong = new JTextField();
		txtCVHinhVuong.setColumns(10);
		txtCVHinhVuong.setBounds(113, 36, 196, 20);
		panel_2.add(txtCVHinhVuong);
		
		JLabel lblDinTch_1 = new JLabel("Diện tích");
		lblDinTch_1.setBounds(10, 67, 93, 14);
		panel_2.add(lblDinTch_1);
		
		txtDTHinhVuong = new JTextField();
		txtDTHinhVuong.setColumns(10);
		txtDTHinhVuong.setBounds(113, 64, 196, 20);
		panel_2.add(txtDTHinhVuong);
		
		JButton btnCVHinhVuong = new JButton("Tính CV");
		btnCVHinhVuong.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				double canh = Double.parseDouble(txtCanh.getText().trim());

				Bai3_Square c = new Bai3_Square(canh);
				txtCVHinhVuong.setText(String.valueOf(c.perimeter()));
			}
		});
		btnCVHinhVuong.setBounds(330, 7, 89, 23);
		panel_2.add(btnCVHinhVuong);
		
		JButton btnDTHinhVuong = new JButton("Tính DT");
		btnDTHinhVuong.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				double canh = Double.parseDouble(txtCanh.getText().trim());

				Bai3_Square c = new Bai3_Square(canh);
				txtDTHinhVuong.setText(String.valueOf(c.area()));
			}
		});
		btnDTHinhVuong.setBounds(330, 35, 89, 23);
		panel_2.add(btnDTHinhVuong);
	}
}
