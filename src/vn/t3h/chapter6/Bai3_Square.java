package vn.t3h.chapter6;

public class Bai3_Square extends Bai3_Shape {

	private double x;

	public Bai3_Square() {
	}

	public Bai3_Square(double x) {
		this.x = x;
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	@Override
	public double perimeter() {
		return x * 4;
	}

	@Override
	public double area() {
		return x * x;
	}

}
