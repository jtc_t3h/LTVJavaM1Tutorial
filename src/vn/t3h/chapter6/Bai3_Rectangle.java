package vn.t3h.chapter6;

public class Bai3_Rectangle extends Bai3_Shape {

	private double x;
	private double y;

	public Bai3_Rectangle() {
	}

	public Bai3_Rectangle(double x, double y) {
		this.x = x;
		this.y = y;
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	@Override
	public double perimeter() {
		return 2 * (x + y);
	}

	@Override
	public double area() {
		return x * y;
	}

}
