package vn.t3h.chapter6;

public class Parent {

	public Parent() {
		System.out.println("Ham khoi tao khong co doi so");
	}
	
	public Parent(String s) {
		System.out.println("Ham khoi tao co 1 doi so");
	}
	
	public Number aMethod() {
		return 0;
	}
	
	public void displayParent() {
		System.out.println("Parent class");
	}
}
