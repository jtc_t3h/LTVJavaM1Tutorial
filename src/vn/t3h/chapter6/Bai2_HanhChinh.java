package vn.t3h.chapter6;

public class Bai2_HanhChinh extends Bai2_NhanVien {
	private double phuCap;

	public Bai2_HanhChinh(double phuCap, double hesoLuong) {
		super(hesoLuong);
		this.phuCap = phuCap;
	}

	public double getPhuCap() {
		return phuCap;
	}

	public void setPhuCap(double phuCap) {
		this.phuCap = phuCap;
	}

	@Override
	public double tinhLuong() {
		return super.tinhLuong() + this.phuCap;
	}
}
