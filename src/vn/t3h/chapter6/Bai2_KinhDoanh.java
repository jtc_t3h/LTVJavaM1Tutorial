package vn.t3h.chapter6;

public class Bai2_KinhDoanh extends Bai2_NhanVien {

	private int soSanPham;

	public Bai2_KinhDoanh(double hesoLuong, int soSanPham) {
		super(hesoLuong);
		this.soSanPham = soSanPham;
	}

	@Override
	public double tinhLuong() {
		return super.tinhLuong() + soSanPham*20000;
	}
	
	
}
