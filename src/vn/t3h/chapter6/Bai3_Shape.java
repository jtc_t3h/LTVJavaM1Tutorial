package vn.t3h.chapter6;

public abstract class Bai3_Shape {
	public abstract double perimeter();

	public abstract double area();
	
	public String  display() {
		return "Abstract class: Shape";
	}
}
