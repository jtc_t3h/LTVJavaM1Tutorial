package vn.t3h.chapter10;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class BtnOpenDlgActionListener implements ActionListener {

	@Override
	public void actionPerformed(ActionEvent e) {
		DlgDemo dlg = new DlgDemo();
		dlg.setVisible(true);
	}

}
