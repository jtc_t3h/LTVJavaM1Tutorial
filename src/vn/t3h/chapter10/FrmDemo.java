package vn.t3h.chapter10;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.border.TitledBorder;
import java.awt.Font;
import java.awt.Color;

public class FrmDemo extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmDemo frame = new FrmDemo();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmDemo() {
		setTitle("Frame demo!");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnOpenDialog = new JButton("Open Dialog");
		btnOpenDialog.addActionListener(new BtnOpenDlgActionListener());
		
		btnOpenDialog.setMnemonic('O');
		btnOpenDialog.setForeground(Color.RED);
		btnOpenDialog.setBackground(Color.ORANGE);
		btnOpenDialog.setFont(new Font("Tahoma", Font.BOLD, 14));
		
		btnOpenDialog.setText("Mo hop thoai!");
		btnOpenDialog.setForeground(Color.BLACK);
		
		btnOpenDialog.setBounds(34, 227, 129, 23);
		contentPane.add(btnOpenDialog);
		
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "Panel demo 1", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.setBounds(10, 11, 414, 94);
		contentPane.add(panel);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(null, "Panel demo 2", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_1.setBounds(10, 116, 414, 78);
		contentPane.add(panel_1);
		
		JButton btnNewButton = new JButton("New button");
		btnNewButton.setBounds(192, 229, 89, 23);
		contentPane.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("New button");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				DlgDemo dlg = new DlgDemo();
				dlg.setVisible(true);
			}
		});
		btnNewButton_1.setBounds(310, 229, 89, 23);
		contentPane.add(btnNewButton_1);
		btnNewButton.addMouseListener(new BtnMouseListener());
	}
}
