package vn.t3h.chapter10;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JRadioButton;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class FrmUser extends JFrame {

	private JPanel contentPane;
	private JTextField txtHoTen;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmUser frame = new FrmUser();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmUser() {
		setTitle("Danh mục người dùng");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 784, 479);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "T\u00ECm ki\u1EBFm", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.setBounds(10, 11, 748, 119);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblHTn = new JLabel("Họ tên");
		lblHTn.setBounds(10, 28, 88, 14);
		panel.add(lblHTn);
		
		txtHoTen = new JTextField();
		txtHoTen.setBounds(108, 25, 387, 20);
		panel.add(txtHoTen);
		txtHoTen.setColumns(10);
		
		JLabel lblPhngBan = new JLabel("Phòng ban");
		lblPhngBan.setBounds(10, 53, 88, 14);
		panel.add(lblPhngBan);
		
		JComboBox cbbPhongBan = new JComboBox();
		cbbPhongBan.setBounds(108, 50, 387, 20);
		panel.add(cbbPhongBan);
		
		JLabel lblGiiTnh = new JLabel("Giới tính");
		lblGiiTnh.setBounds(10, 81, 88, 14);
		panel.add(lblGiiTnh);
		
		JRadioButton rdbtnNam = new JRadioButton("Nam");
		rdbtnNam.setBounds(118, 77, 109, 23);
		panel.add(rdbtnNam);
		
		JRadioButton rdbtnNu = new JRadioButton("Nữ");
		rdbtnNu.setBounds(239, 77, 109, 23);
		panel.add(rdbtnNu);
		
		JRadioButton rdbtnKhac = new JRadioButton("Khác");
		rdbtnKhac.setBounds(350, 77, 109, 23);
		panel.add(rdbtnKhac);
		
		JButton btnSearch = new JButton("Tìm kiếm");
		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			}
		});
		btnSearch.setIcon(new ImageIcon(FrmUser.class.getResource("/vn/t3h/resources/search-icon-hi.png")));
		btnSearch.setBounds(520, 28, 133, 67);
		panel.add(btnSearch);
//		btnSearch.addActionListener(new BtnTimKiemActionListener(txtHoTen.getText(), cbbPhongBan.getSelectedIndex(), 
//				rdbtnNam.isSelected()?"0":(rdbtnNu.isSelected()?"1":"2")));
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(null, "Danh s\u00E1ch ng\u01B0\u1EDDi d\u00F9ng", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_1.setBounds(10, 141, 748, 288);
		contentPane.add(panel_1);
		panel_1.setLayout(null);
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null, null},
			},
			new String[] {
				"New column", "New column", "New column", "New column", "New column"
			}
		));
		table.setBounds(10, 22, 728, 255);
		panel_1.add(table);
	}
}
