package vn.t3h.chapter5;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.HeadlessException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class FrmBai4_ShoppingCart2 extends JFrame {

	private JPanel contentPane;
	private JTable table;
	
	private Bai4_Product listOfProduct[];
	private int listOfSelect[];

	/**
	 * Launch the application.
	 */
//	public static void main(String[] args) {
//		EventQueue.invokeLater(new Runnable() {
//			public void run() {
//				try {
//					FrmBai4_ShoppingCart2 frame = new FrmBai4_ShoppingCart2();
//					frame.setVisible(true);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		});
//	}

	/**
	 * Create the frame.
	 */
	public FrmBai4_ShoppingCart2(Bai4_Product[] listOfProduct, int[] listOfSelect) {
		this.listOfProduct = listOfProduct;
		this.listOfSelect = listOfSelect;
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 11, 414, 188);
		contentPane.add(scrollPane);
		
		Object[][] dataTableTmp = new Object[4][2];
		int rowSize = 0;
		for(int idx = 0; idx < listOfSelect.length; idx++) {
			if (listOfSelect[idx] == 1) {
				dataTableTmp[rowSize] = new Object[2];
				dataTableTmp[rowSize][0] =	listOfProduct[idx].getName();
				dataTableTmp[rowSize][1] =	listOfProduct[idx].getPrice();
				rowSize++;
			}
		}
		
		Object[][] dataTable = new Object[rowSize][2];
		for (int idx = 0; idx < rowSize; idx++) {
			dataTable[idx][0] = dataTableTmp[idx][0];
			dataTable[idx][1] = dataTableTmp[idx][1];
		}
		
		table = new JTable();		
		table.setModel(new DefaultTableModel(
			dataTable,
			new String[] {
				"Product", "Price"
			}
		));
		scrollPane.setViewportView(table);
		
		JButton btnPayment = new JButton("Payment");
		btnPayment.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				FrmBai4_ShoppingCart3 frm = new FrmBai4_ShoppingCart3();
				frm.setVisible(true);
			}
		});
		btnPayment.setBounds(149, 210, 126, 39);
		contentPane.add(btnPayment);
	}
	
}
