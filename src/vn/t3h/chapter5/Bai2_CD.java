package vn.t3h.chapter5;

public class Bai2_CD {

	private String maCD;
	private String tenCD;
	private String caSi;
	private int soBaiHat;
	private int giaThanh;

	private static int tongGiaThanh = 0;

	public Bai2_CD(String maCD, String tenCD, String caSi, int soBaiHat, int giaThanh) {
		this.maCD = maCD;
		this.tenCD = tenCD;
		this.caSi = caSi;
		this.soBaiHat = soBaiHat;
		this.giaThanh = giaThanh;
		
		tongGiaThanh += giaThanh;
	}

	public String getMaCD() {
		return maCD;
	}

	public void setMaCD(String maCD) {
		this.maCD = maCD;
	}

	public String getTenCD() {
		return tenCD;
	}

	public void setTenCD(String tenCD) {
		this.tenCD = tenCD;
	}

	public String getCaSi() {
		return caSi;
	}

	public void setCaSi(String caSi) {
		this.caSi = caSi;
	}

	public int getSoBaiHat() {
		return soBaiHat;
	}

	public void setSoBaiHat(int soBaiHat) {
		this.soBaiHat = soBaiHat;
	}

	public int getGiaThanh() {
		return giaThanh;
	}

	public void setGiaThanh(int giaThanh) {
		this.giaThanh = giaThanh;
	}

	public static int getTongGiaThanh() {
		return tongGiaThanh;
	}

	public static void setTongGiaThanh(int tongGiaThanh) {
		Bai2_CD.tongGiaThanh = tongGiaThanh;
	}

	@Override
	public String toString() {
		return maCD + " - " + tenCD + " - " + caSi + " - " + soBaiHat + " - " + giaThanh;
	}

}
