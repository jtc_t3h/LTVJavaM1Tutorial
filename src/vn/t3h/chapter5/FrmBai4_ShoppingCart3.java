package vn.t3h.chapter5;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JButton;

public class FrmBai4_ShoppingCart3 extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai4_ShoppingCart3 frame = new FrmBai4_ShoppingCart3();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai4_ShoppingCart3() {
		setTitle("Payment infomation");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 277);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblCa = new JLabel("Cart type");
		lblCa.setBounds(70, 11, 69, 14);
		contentPane.add(lblCa);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setBounds(149, 8, 182, 20);
		contentPane.add(comboBox);
		
		JLabel lblCardNumber = new JLabel("Card number");
		lblCardNumber.setBounds(70, 41, 69, 14);
		contentPane.add(lblCardNumber);
		
		textField = new JTextField();
		textField.setBounds(149, 35, 182, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JLabel lblBillInfomation = new JLabel("Bill Infomation");
		lblBillInfomation.setHorizontalAlignment(SwingConstants.CENTER);
		lblBillInfomation.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblBillInfomation.setBounds(143, 66, 121, 14);
		contentPane.add(lblBillInfomation);
		
		JLabel lblFullName = new JLabel("Full Name");
		lblFullName.setBounds(70, 94, 69, 14);
		contentPane.add(lblFullName);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(149, 91, 182, 20);
		contentPane.add(textField_1);
		
		JLabel lblAddress = new JLabel("Address");
		lblAddress.setBounds(70, 122, 69, 14);
		contentPane.add(lblAddress);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(149, 119, 182, 20);
		contentPane.add(textField_2);
		
		JLabel lblCountry = new JLabel("Country");
		lblCountry.setBounds(70, 150, 69, 14);
		contentPane.add(lblCountry);
		
		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(149, 147, 182, 20);
		contentPane.add(textField_3);
		
		JLabel lblZip = new JLabel("Zip Code");
		lblZip.setBounds(70, 178, 69, 14);
		contentPane.add(lblZip);
		
		textField_4 = new JTextField();
		textField_4.setColumns(10);
		textField_4.setBounds(149, 175, 182, 20);
		contentPane.add(textField_4);
		
		JButton btnSubmit = new JButton("Submit");
		btnSubmit.setBounds(149, 206, 89, 23);
		contentPane.add(btnSubmit);
	}

}
