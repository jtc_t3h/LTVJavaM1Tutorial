package vn.t3h.chapter5;

public class TestClient {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// Tao doi tuong lop -> object <> instance
//		People p = null; // p: object
//		p = new People(); // p: instance
//		
//		System.out.println(p.id); // 0
////		System.out.println(p.name);
		
		int i = 1;
		int arr[] = {1};
		
		changeValue(i);
		changeValue(arr);
		
		System.out.println(i); // 1 -> truyen tham tri | 11 -> truyen tham bien -> 1
		System.out.println(arr[0]); // 1 | 11 -> 11
	}

	public static void changeValue(int i) {
		i = i + 10;
	}
	
	public static void changeValue (int [] arr) {
		arr[0] = arr[0] + 10;
	}
	
}
