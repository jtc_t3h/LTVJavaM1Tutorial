package vn.t3h.chapter5;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JList;
import javax.swing.JTextArea;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class FrmBai2_QuanLyCD extends JFrame {

	private JPanel contentPane;
	private JTextField txtMaCD;
	private JTextField txtTenCD;
	private JTextField txtCaSi;
	private JTextField txtSoBaiHat;
	private JTextField txtGiaThanh;
	private JTextArea taCD;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai2_QuanLyCD frame = new FrmBai2_QuanLyCD();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai2_QuanLyCD() {
		setTitle("Qu\u1EA3n l\u00FD CD");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 536, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblMCd = new JLabel("M\u00E3 CD");
		lblMCd.setBounds(10, 11, 46, 14);
		contentPane.add(lblMCd);
		
		txtMaCD = new JTextField();
		txtMaCD.setBounds(66, 8, 98, 20);
		contentPane.add(txtMaCD);
		txtMaCD.setColumns(10);
		
		JLabel lblTnCd = new JLabel("T\u00EAn CD");
		lblTnCd.setBounds(174, 11, 46, 14);
		contentPane.add(lblTnCd);
		
		txtTenCD = new JTextField();
		txtTenCD.setBounds(228, 8, 282, 20);
		contentPane.add(txtTenCD);
		txtTenCD.setColumns(10);
		
		JLabel lblTnCa = new JLabel("Ca s\u0129");
		lblTnCa.setBounds(10, 42, 46, 14);
		contentPane.add(lblTnCa);
		
		txtCaSi = new JTextField();
		txtCaSi.setBounds(66, 39, 162, 20);
		contentPane.add(txtCaSi);
		txtCaSi.setColumns(10);
		
		JLabel lblSBiHt = new JLabel("S\u1ED1 b\u00E0i h\u00E1t");
		lblSBiHt.setBounds(238, 42, 58, 14);
		contentPane.add(lblSBiHt);
		
		txtSoBaiHat = new JTextField();
		txtSoBaiHat.setBounds(306, 39, 46, 20);
		contentPane.add(txtSoBaiHat);
		txtSoBaiHat.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Gi\u00E1 th\u00E0nh");
		lblNewLabel.setBounds(362, 42, 58, 14);
		contentPane.add(lblNewLabel);
		
		txtGiaThanh = new JTextField();
		txtGiaThanh.setBounds(424, 39, 86, 20);
		contentPane.add(txtGiaThanh);
		txtGiaThanh.setColumns(10);
		
		JButton btnThemMoi = new JButton("Th\u00EAm m\u1EDBi");
		btnThemMoi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				// Lay du lieu nguoi dung nhap
				String maCD = txtMaCD.getText().trim();
				String tenCD = txtTenCD.getText().trim();
				String caSi = txtCaSi.getText().trim();
				int soBaiHat = Integer.parseInt(txtSoBaiHat.getText().trim());
				int giaThanh = Integer.parseInt(txtGiaThanh.getText().trim());
				
				// Tao ra doi tuong CD
				Bai2_CD cd = new Bai2_CD(maCD, tenCD, caSi, soBaiHat, giaThanh);
				
				String strHienThi = taCD.getText();
				if (!strHienThi.isEmpty()) {
					strHienThi = strHienThi.substring(0, strHienThi.indexOf("\r\n"));
					strHienThi += cd.toString() + "\n";
				} else {
					strHienThi = cd.toString() + "\n";
				}
				strHienThi += "\r\n" + "Tổng giá thành = " + Bai2_CD.getTongGiaThanh(); 
				
				taCD.setText(strHienThi);
			}
		});
		btnThemMoi.setBounds(162, 77, 103, 23);
		contentPane.add(btnThemMoi);
		
		JButton btnTiepTuc = new JButton("Ti\u1EBFp t\u1EE5c");
		btnTiepTuc.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txtMaCD.setText("");
				txtTenCD.setText("");
				txtCaSi.setText("");
				txtSoBaiHat.setText("");
				txtGiaThanh.setText("");
			}
		});
		btnTiepTuc.setBounds(278, 77, 98, 23);
		contentPane.add(btnTiepTuc);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 110, 500, 140);
		contentPane.add(scrollPane);
		
		taCD = new JTextArea();
		scrollPane.setViewportView(taCD);
	}
}
