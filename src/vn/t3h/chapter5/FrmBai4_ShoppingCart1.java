package vn.t3h.chapter5;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class FrmBai4_ShoppingCart1 extends JFrame {

	private JPanel contentPane;
	
	private Bai4_Product listOfProduct[] = new Bai4_Product[4];
	private int listOfSelect[] = {0, 0, 0, 0};

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai4_ShoppingCart1 frame = new FrmBai4_ShoppingCart1();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai4_ShoppingCart1() {
		setTitle("Click to buy");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 496);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblProduct1 = new JLabel("");
		lblProduct1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if (listOfSelect[0] == 0) {
					listOfSelect[0] = 1;
				} else {
					listOfSelect[0] = 0;
				}
			}
		});
		lblProduct1.setIcon(new ImageIcon(FrmBai4_ShoppingCart1.class.getResource("/vn/t3h/resources/Whey-stand.jpg")));
		lblProduct1.setBounds(10, 11, 200, 200);
		contentPane.add(lblProduct1);
		
		JLabel lblProduct2 = new JLabel("");
		lblProduct2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (listOfSelect[1] == 0) {
					listOfSelect[1] = 1;
				} else {
					listOfSelect[1] = 0;
				}
			}
		});
		lblProduct2.setIcon(new ImageIcon(FrmBai4_ShoppingCart1.class.getResource("/vn/t3h/resources/anabolic-whey.jpg")));
		lblProduct2.setBounds(224, 11, 200, 200);
		contentPane.add(lblProduct2);
		
		JLabel lblProduct3 = new JLabel("");
		lblProduct3.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (listOfSelect[2] == 0) {
					listOfSelect[2] = 1;
				} else {
					listOfSelect[2] = 0;
				}
			}
		});
		lblProduct3.setIcon(new ImageIcon(FrmBai4_ShoppingCart1.class.getResource("/vn/t3h/resources/Hydro-Whey-1590-.jpg")));
		lblProduct3.setBounds(10, 223, 200, 200);
		contentPane.add(lblProduct3);
		
		JLabel lblProduct4 = new JLabel("");
		lblProduct4.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (listOfSelect[3] == 0) {
					listOfSelect[3] = 1;
				} else {
					listOfSelect[3] = 0;
				}
			}
		});
		lblProduct4.setIcon(new ImageIcon(FrmBai4_ShoppingCart1.class.getResource("/vn/t3h/resources/bsn-syntha-6-whey.jpg")));
		lblProduct4.setBounds(224, 222, 200, 200);
		contentPane.add(lblProduct4);
		
		JButton btnCart = new JButton("Cart");
		btnCart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (!isSelect()) {
					JOptionPane.showMessageDialog(null, "Please select any product!");
					return;
				}
				
				if (JOptionPane.showConfirmDialog(null, "Do you want to buy?") == JOptionPane.YES_OPTION) {
					FrmBai4_ShoppingCart2 frm = new FrmBai4_ShoppingCart2(listOfProduct, listOfSelect);
					frm.setVisible(true);
				}
			}
		});
		btnCart.setIcon(new ImageIcon(FrmBai4_ShoppingCart1.class.getResource("/vn/t3h/resources/Cart-icon.png")));
		btnCart.setBounds(0, 434, 434, 23);
		contentPane.add(btnCart);
		
		listOfProduct();
	}

	protected boolean isSelect() {
		for (int element: listOfSelect) {
			if (element == 1) {
				return true;
			}
		}
		return false;
	}

	private void listOfProduct() {
		listOfProduct[0] = new Bai4_Product("WHEY GOLD STANDARD", 60);
		listOfProduct[1] = new Bai4_Product("ANABOLIC WHEY", 90);
		listOfProduct[2] = new Bai4_Product("PLATINUM HYDRO WHEY", 120);
		listOfProduct[3] = new Bai4_Product("SYNTHA-6", 200);
	}

}
