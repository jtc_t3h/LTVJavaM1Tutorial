package vn.t3h.chapter5;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;

public class FrmBai1_PhanSo extends JFrame {

	private JPanel contentPane;
	private JTextField txtTuSo1;
	private JTextField txtMauSo1;
	private JTextField txtTuSo2;
	private JTextField txtMauSo2;
	private JTextField txtKetQua;
	private JComboBox cbbOperator;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai1_PhanSo frame = new FrmBai1_PhanSo();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai1_PhanSo() {
		setTitle("T\u00EDnh to\u00E1n ph\u00E2n s\u1ED1");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 436, 238);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblPhnS = new JLabel("Ph\u00E2n s\u1ED1 1");
		lblPhnS.setBounds(64, 11, 70, 14);
		contentPane.add(lblPhnS);
		
		JLabel lblTS = new JLabel("T\u1EED s\u1ED1:");
		lblTS.setBounds(10, 38, 46, 14);
		contentPane.add(lblTS);
		
		txtTuSo1 = new JTextField();
		txtTuSo1.setBounds(64, 36, 86, 20);
		contentPane.add(txtTuSo1);
		txtTuSo1.setColumns(10);
		
		txtMauSo1 = new JTextField();
		txtMauSo1.setColumns(10);
		txtMauSo1.setBounds(64, 63, 86, 20);
		contentPane.add(txtMauSo1);
		
		JLabel lblMuS = new JLabel("M\u1EABu s\u1ED1:");
		lblMuS.setBounds(10, 65, 46, 14);
		contentPane.add(lblMuS);
		
		JLabel lblPhnS_1 = new JLabel("Ph\u00E2n s\u1ED1 2");
		lblPhnS_1.setBounds(316, 11, 70, 14);
		contentPane.add(lblPhnS_1);
		
		JLabel label_2 = new JLabel("T\u1EED s\u1ED1:");
		label_2.setBounds(262, 38, 46, 14);
		contentPane.add(label_2);
		
		txtTuSo2 = new JTextField();
		txtTuSo2.setColumns(10);
		txtTuSo2.setBounds(316, 36, 86, 20);
		contentPane.add(txtTuSo2);
		
		txtMauSo2 = new JTextField();
		txtMauSo2.setColumns(10);
		txtMauSo2.setBounds(316, 63, 86, 20);
		contentPane.add(txtMauSo2);
		
		JLabel lblMuS_1 = new JLabel("M\u1EABu s\u1ED1:");
		lblMuS_1.setBounds(262, 65, 46, 14);
		contentPane.add(lblMuS_1);
		
		cbbOperator = new JComboBox();
		cbbOperator.setModel(new DefaultComboBoxModel(new String[] {"", "+", "-", "*", "/"}));
		cbbOperator.setBounds(187, 48, 54, 20);
		contentPane.add(cbbOperator);
		
		JLabel lblKtQu = new JLabel("K\u1EBFt qu\u1EA3");
		lblKtQu.setBounds(10, 112, 46, 14);
		contentPane.add(lblKtQu);
		
		txtKetQua = new JTextField();
		txtKetQua.setBounds(64, 109, 125, 20);
		contentPane.add(txtKetQua);
		txtKetQua.setColumns(10);
		
		JButton btnTinh = new JButton("T\u00EDnh");
		btnTinh.setBounds(64, 150, 89, 23);
		contentPane.add(btnTinh);
		btnTinh.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println(txtTuSo1.getText().trim());
				int tuSo1 = Integer.parseInt(txtTuSo1.getText().trim());
				int mauSo1 = Integer.parseInt(txtMauSo1.getText().trim());
				Bai1_PhanSo phanSo1 = new Bai1_PhanSo(tuSo1, mauSo1);
				
				int tuSo2 = Integer.parseInt(txtTuSo2.getText().trim());
				int mauSo2 = Integer.parseInt(txtMauSo2.getText().trim());
				Bai1_PhanSo phanSo2 = new Bai1_PhanSo(tuSo2, mauSo2);
				
				String operator = (String) cbbOperator.getSelectedItem();
				Bai1_PhanSo phanSoKetQua = null;
				if ("+".equals(operator)) {
					phanSoKetQua = phanSo1.add(phanSo2);
				} else if ("-".equals(operator)) {
					phanSoKetQua = phanSo1.subtract(phanSo2);
				} else if ("*".equals(operator)) {
					phanSoKetQua = phanSo1.multiply(phanSo2);
				} else if ("/".equals(operator)) {
					phanSoKetQua = phanSo1.devide(phanSo2);
				}
				
				txtKetQua.setText(phanSoKetQua.toString());
			}
		});
		
		JButton btnLamLai = new JButton("L\u00E0m L\u1EA1i");
		btnLamLai.setBounds(193, 150, 89, 23);
		contentPane.add(btnLamLai);
		btnLamLai.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				txtTuSo1.setText("");
				txtMauSo1.setText("");
				txtTuSo2.setText("");
				txtMauSo2.setText("");
				cbbOperator.setSelectedIndex(0);
				txtKetQua.setText("");
			}
		});
	}
}
