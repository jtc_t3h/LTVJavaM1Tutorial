package vn.t3h.chapter5;

public class Bai1_PhanSo {

	private int tuSo;
	private int mauSo;

	public Bai1_PhanSo(int tuSo, int mauSo) {
		this.tuSo = tuSo;
		this.mauSo = mauSo;
	}

	public int getTuSo() {
		return tuSo;
	}

	public void setTuSo(int tuSo) {
		this.tuSo = tuSo;
	}

	public int getMauSo() {
		return mauSo;
	}

	public void setMauSo(int mauSo) {
		this.mauSo = mauSo;
	}
	
	public Bai1_PhanSo add(Bai1_PhanSo phanSo2) {
		int kqTuSo = tuSo * phanSo2.getMauSo() + mauSo * phanSo2.getTuSo();
		int kqMauSo = mauSo * phanSo2.getMauSo();
		
		return new Bai1_PhanSo(kqTuSo, kqMauSo);
	}
	
	public Bai1_PhanSo subtract(Bai1_PhanSo phanSo2) {
		int kqTuSo = tuSo * phanSo2.getMauSo() - mauSo * phanSo2.getTuSo();
		int kqMauSo = mauSo * phanSo2.getMauSo();
		
		return new Bai1_PhanSo(kqTuSo, kqMauSo);
	}
	
	public Bai1_PhanSo multiply(Bai1_PhanSo phanSo2) {
		int kqTuSo = tuSo * phanSo2.getTuSo();
		int kqMauSo = mauSo * phanSo2.getMauSo();
		
		return new Bai1_PhanSo(kqTuSo, kqMauSo);
	}
	
	public Bai1_PhanSo devide(Bai1_PhanSo phanSo2) {
		int kqTuSo = tuSo * phanSo2.getMauSo() ;
		int kqMauSo = mauSo * phanSo2.getTuSo();
		
		return new Bai1_PhanSo(kqTuSo, kqMauSo);
	}

	@Override
	public String toString() {
		return tuSo + "/" + mauSo;
	}
	
	

}
