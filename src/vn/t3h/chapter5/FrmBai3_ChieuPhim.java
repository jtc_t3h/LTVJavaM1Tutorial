package vn.t3h.chapter5;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;

public class FrmBai3_ChieuPhim extends JFrame {

	private JPanel contentPane;
	private JTextField txtTenPhim;
	private JTextField txtTheLoai;
	private JTextField txtNgayChieu;
	private JTextField txtRap;
	
	private int flag = 0;
	private Bai3_Phim data[] = new Bai3_Phim[5];
	private JLabel lblImage;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai3_ChieuPhim frame = new FrmBai3_ChieuPhim();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai3_ChieuPhim() {
		setTitle("Phim chi\u1EBFu r\u1EA1p");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 571, 325);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnPrevious = new JButton("");
		btnPrevious.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (flag == 0) {
					JOptionPane.showMessageDialog(null, "Đây là phim đầu tiên. Vui lòng nhấn => để xem các phim tiếp theo!");
				} else {
					flag--;
					lblImage.setIcon(new ImageIcon(getClass().getResource(data[flag].getDuongDanHinh())));
					txtTenPhim.setText(data[flag].getTenPhim());
					txtTheLoai.setText(data[flag].getTheLoai());
					txtNgayChieu.setText(data[flag].getNgayChieu());
					txtRap.setText(data[flag].getRap());
				}
			}
		});
		btnPrevious.setIcon(new ImageIcon(FrmBai3_ChieuPhim.class.getResource("/vn/t3h/resources/previous-icon.png")));
		btnPrevious.setBounds(10, 132, 39, 23);
		contentPane.add(btnPrevious);
		
		lblImage = new JLabel("");
		lblImage.setIcon(new ImageIcon(FrmBai3_ChieuPhim.class.getResource("/vn/t3h/resources/bietdoichimcanhcut.jpg")));
		lblImage.setBounds(59, 11, 194, 264);
		contentPane.add(lblImage);
		
		txtTenPhim = new JTextField();
		txtTenPhim.setBackground(Color.YELLOW);
		txtTenPhim.setBounds(291, 42, 189, 20);
		contentPane.add(txtTenPhim);
		txtTenPhim.setColumns(10);
		
		JButton btnNext = new JButton("");
		btnNext.setIcon(new ImageIcon(FrmBai3_ChieuPhim.class.getResource("/vn/t3h/resources/next-icon.png")));
		btnNext.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (flag == 4) {
					JOptionPane.showMessageDialog(null, "Đã hết phim. Vui lòng nhấn <= để xem các phim phía trước!");
				} else {
					flag++;
					lblImage.setIcon(new ImageIcon(FrmBai3_ChieuPhim.class.getResource(data[flag].getDuongDanHinh())));
					txtTenPhim.setText(data[flag].getTenPhim());
					txtTheLoai.setText(data[flag].getTheLoai());
					txtNgayChieu.setText(data[flag].getNgayChieu());
					txtRap.setText(data[flag].getRap());
				}
			}
		});
		btnNext.setBounds(501, 132, 39, 23);
		contentPane.add(btnNext);
		
		txtTheLoai = new JTextField();
		txtTheLoai.setBackground(Color.ORANGE);
		txtTheLoai.setColumns(10);
		txtTheLoai.setBounds(291, 100, 189, 20);
		contentPane.add(txtTheLoai);
		
		txtNgayChieu = new JTextField();
		txtNgayChieu.setBackground(Color.GREEN);
		txtNgayChieu.setColumns(10);
		txtNgayChieu.setBounds(291, 161, 189, 20);
		contentPane.add(txtNgayChieu);
		
		txtRap = new JTextField();
		txtRap.setBackground(Color.PINK);
		txtRap.setColumns(10);
		txtRap.setBounds(291, 227, 189, 20);
		contentPane.add(txtRap);
		
		dsPhim();
		initData();
	}

	private void initData() {
		lblImage.setIcon(new ImageIcon(getClass().getResource(data[0].getDuongDanHinh())));
		txtTenPhim.setText(data[0].getTenPhim());
		txtTheLoai.setText(data[0].getTheLoai());
		txtNgayChieu.setText(data[0].getNgayChieu());
		txtRap.setText(data[0].getRap());
	}

	private void dsPhim() {
		data[0] = new Bai3_Phim("Biệt đội chim cánh cụt", "Hoạt hình",  "25/12/2014", "CineBox Hòa Bình", "/vn/t3h/resources/bietdoichimcanhcut.jpg");
		data[1] = new Bai3_Phim("Big Heros 6", "Hoạt hình", "10/01/2015", "Galaxy Nguyễn Du", "/vn/t3h/resources/bighero6.jpg");
		data[2] = new Bai3_Phim("Chàng trai năm ấy", "Tình cảm", "15/01/2015", "CineBox Lý Chính Thắng", "/vn/t3h/resources/changtrainamay.jpg");
		data[3] = new Bai3_Phim("Cuộc chiến chống Pharaon", "Giả sử", "20/01/2015", "Galaxy Nguyễn Trãi", "/vn/t3h/resources/cuocchienchongpharaon.jpg");
		data[4] = new Bai3_Phim("Để mai tính", "Hài", "25/01/2015", "Galaxy Quang Trung", "/vn/t3h/resources/demaitinh.jpg");
	}

}
