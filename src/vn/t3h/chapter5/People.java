package vn.t3h.chapter5;

public class People { // Top-level class

	// Bien thuc the
	public int id;
	private String name;
	private int bitrth;
	private boolean gender; // false = women, true = men

	// Ham khoi tao mac dinh (khong co tham so)
	public People() {
		id = 0;
		name = null;
	}

	// Ham khoi tao co tham so
	public People(int id1) {
		id = id1;
	}

	public People(int id1, String name1) {
		id = id1;
		name = name1;
	}

	public People(int id, String name, int bitrth, boolean gender) {
		super();
		this.id = id;
		this.name = name;
		this.bitrth = bitrth;
		this.gender = gender;
	}

	// Ham getter
	public int getId() {
		return id;
	}

	// Ham setter
	public void setId(int _id) {
		id = _id;
	}

	public void setBirth(int _birth) {
		if (_birth < 1980) {
			bitrth = 0;
		} else {
			bitrth = _birth;
		}

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getBitrth() {
		return bitrth;
	}

	public void setBitrth(int bitrth) {
		this.bitrth = bitrth;
	}

	public boolean isGender() {
		return gender;
	}

	public void setGender(boolean gender) {
		this.gender = gender;
	}

	// Ham nghiep vu
	public void displayInfo() {
		System.out.println("Id: " + id);
		System.out.println("Nam: " + name);
	}

}
