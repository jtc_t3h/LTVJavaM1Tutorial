package vn.t3h.chapter7;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JList;
import javax.swing.AbstractListModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTabbedPane;

public class FrmListAndTextArea extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmListAndTextArea frame = new FrmListAndTextArea();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmListAndTextArea() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 616, 441);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 11, 270, 150);
		contentPane.add(scrollPane);
		
		JTextArea txtrRqTgqyqyhqwyuqotbtqhgVgraehgkawhgawrgh = new JTextArea();
		txtrRqTgqyqyhqwyuqotbtqhgVgraehgkawhgawrgh.setText("r34q tg3q4y34qyh3qwyu42qotb2tq3hg\r\nvgraehgkawhgawrgh");
		scrollPane.setRowHeaderView(txtrRqTgqyqyhqwyuqotbtqhgVgraehgkawhgawrgh);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(10, 172, 586, 194);
		contentPane.add(scrollPane_1);
		
		JList list = new JList();
		scrollPane_1.setViewportView(list);
		
		JButton btnInitDataTextarea = new JButton("Init Data TextArea");
		btnInitDataTextarea.setBounds(59, 377, 167, 23);
		contentPane.add(btnInitDataTextarea);
		
		JButton btnInitDataJlist = new JButton("Init Data JList");
		btnInitDataJlist.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				// Tao ra 1 mang 
				String [] arr = {"itemn 1", "item 2"};
				
				// Tao ra doi tuong DefaultList
				DefaultComboBoxModel model = new DefaultComboBoxModel(arr);
				
				// Thiet lap thuoc tinh model
				list.setModel(model);
				
			}
		});
		btnInitDataJlist.setBounds(358, 377, 116, 23);
		contentPane.add(btnInitDataJlist);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(290, 11, 306, 150);
		contentPane.add(tabbedPane);
		
		JPanel panel = new JPanel();
		tabbedPane.addTab("New tab", null, panel, null);
		
		JPanel panel_1 = new JPanel();
		tabbedPane.addTab("New tab", null, panel_1, null);
		
		JPanel panel_2 = new JPanel();
		tabbedPane.addTab("New tab", null, panel_2, null);
	}
}
