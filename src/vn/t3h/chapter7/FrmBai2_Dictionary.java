package vn.t3h.chapter7;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTabbedPane;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.awt.event.ActionEvent;

public class FrmBai2_Dictionary extends JFrame {

	private JPanel contentPane;
	private JTextField txtWord;
	private JTextField txtMeaning;
	private JTextField txtKeyword;
	private JTextField txtResult;

	List<Bai2_Dictionary> listDic = new ArrayList<>();
	private JList lstWord;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai2_Dictionary frame = new FrmBai2_Dictionary();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai2_Dictionary() {
		setTitle("Dictionary");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 533, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(0, 0, 517, 261);
		contentPane.add(tabbedPane);

		JPanel panel = new JPanel();
		tabbedPane.addTab("Show - Insert word", null, panel, null);
		panel.setLayout(null);

		JLabel lblWord = new JLabel("Word");
		lblWord.setBounds(10, 11, 40, 14);
		panel.add(lblWord);

		txtWord = new JTextField();
		txtWord.setBounds(60, 8, 137, 20);
		panel.add(txtWord);
		txtWord.setColumns(10);

		JLabel lblMeaning = new JLabel("Meaning");
		lblMeaning.setBounds(207, 11, 59, 14);
		panel.add(lblMeaning);

		txtMeaning = new JTextField();
		txtMeaning.setBounds(274, 8, 145, 20);
		panel.add(txtMeaning);
		txtMeaning.setColumns(10);

		JButton btnInsert = new JButton("Insert");
		btnInsert.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Bai2_Dictionary dic = new Bai2_Dictionary(txtWord.getText(), txtMeaning.getText());

				listDic.add(dic);
				Collections.sort(listDic);

				DefaultListModel model = new DefaultListModel();
				int i = 0;
				for (Bai2_Dictionary x : listDic) {
					Object t = new String(x.getWord() + " : " + x.getMeaning());
					model.add(i, t);
					i++;
				}
				lstWord.setModel(model);
			}
		});
		btnInsert.setBounds(429, 7, 73, 23);
		panel.add(btnInsert);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 36, 492, 186);
		panel.add(scrollPane);

		lstWord = new JList();
		scrollPane.setViewportView(lstWord);

		JPanel panel_1 = new JPanel();
		tabbedPane.addTab("Search", null, panel_1, null);
		panel_1.setLayout(null);

		JLabel lblWord_1 = new JLabel("Word");
		lblWord_1.setBounds(10, 11, 46, 14);
		panel_1.add(lblWord_1);

		txtKeyword = new JTextField();
		txtKeyword.setBounds(96, 8, 199, 20);
		panel_1.add(txtKeyword);
		txtKeyword.setColumns(10);

		JLabel lblMeaning_1 = new JLabel("Meaning");
		lblMeaning_1.setBounds(10, 49, 76, 14);
		panel_1.add(lblMeaning_1);

		txtResult = new JTextField();
		txtResult.setColumns(10);
		txtResult.setBounds(96, 46, 199, 20);
		panel_1.add(txtResult);

		JButton btnSearch = new JButton("");
		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String word = new String(txtKeyword.getText());
				
				String meaning = "";
				boolean result = false;
				for (Bai2_Dictionary d : listDic) {
					if (d.getWord().equals(word)) {
						result = true;
						meaning = d.getMeaning();
						break;
					}
				}
				if (!result) {
					JOptionPane.showMessageDialog(rootPane, "Sorry, cannot find this word!");
				}
				txtResult.setText(meaning);
			}
		});
		btnSearch.setIcon(new ImageIcon(FrmBai2_Dictionary.class.getResource("/vn/t3h/resources/search-icon-hi.png")));
		btnSearch.setBounds(305, 7, 89, 59);
		panel_1.add(btnSearch);
	}

}
