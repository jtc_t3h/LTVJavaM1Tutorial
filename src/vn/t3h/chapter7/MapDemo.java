package vn.t3h.chapter7;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class MapDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Map map = new HashMap<>();
		
		map.put(123, "Hello");
		map.put("hello", 123);
		map.put(123,"Hello world");
		map.put(null, 12345);

		System.out.println(map.size());
		
//		for (Object key: map.keySet()) {
//			System.out.println(map.get(key));
//		}
		
		
		Set set =  map.entrySet();
		for (Object element: set) {
			Entry entry = (Entry) element;
			System.out.println(entry.getKey() + " = " + entry.getValue());
		}
		
	}

}
