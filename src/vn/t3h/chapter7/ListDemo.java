package vn.t3h.chapter7;

import java.util.ArrayList;
import java.util.List;

public class ListDemo {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void main(String[] args) {

		List list = new ArrayList();
		
		// them phan tu vao List
		list.add("Hello");
		list.add(123);
		list.add('a');
		list.add(1.2);
		
		// duyet cac phan tu cua List -> duyet cac phan tu cua tap hop
		for (Object element: list) {
			System.out.println("element = " + element);
		}
	}

}
