package vn.t3h.chapter7;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

public class SetDemo {

	public static void main(String[] args) {
		Set set = new HashSet<>();

		set.add(12);
		set.add("Hello");
		set.add("Hello");

		System.out.println("size = " + set.size());
		for (Object element : set) {
			System.out.println(element);
		}

		SortedSet<String> sortedSet = new TreeSet();

		sortedSet.add("Hello");
		sortedSet.add("AAA");

		for (Object element : sortedSet) {
			System.out.println(element);
		}
		
		
	}
}
