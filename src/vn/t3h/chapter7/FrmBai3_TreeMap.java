package vn.t3h.chapter7;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.util.Map;
import java.util.TreeMap;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JList;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class FrmBai3_TreeMap extends JFrame {

	private JPanel contentPane;
	private JTextField txtKey;
	private JTextField txtValue;

	Map DanhSach = new TreeMap();
	private JList list;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai3_TreeMap frame = new FrmBai3_TreeMap();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai3_TreeMap() {
		setTitle("Tree Map");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblKey = new JLabel("Key");
		lblKey.setBounds(10, 11, 46, 14);
		contentPane.add(lblKey);

		txtKey = new JTextField();
		txtKey.setBounds(66, 8, 129, 20);
		contentPane.add(txtKey);
		txtKey.setColumns(10);

		JLabel lblValue = new JLabel("Value");
		lblValue.setBounds(205, 11, 46, 14);
		contentPane.add(lblValue);

		txtValue = new JTextField();
		txtValue.setBounds(261, 8, 163, 20);
		contentPane.add(txtValue);
		txtValue.setColumns(10);

		JButton btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DanhSach.put(txtKey.getText(), txtValue.getText());

				DefaultListModel model = new DefaultListModel();
				int i = 0;
				for (Object key : DanhSach.keySet()) {
					Object value = new String(key.toString() + " : " + (String) DanhSach.get(key));
					model.add(i, value);
					i++;
				}
				list.setModel(model);
			}
		});
		btnAdd.setBounds(66, 48, 89, 23);
		contentPane.add(btnAdd);

		JButton btnContinue = new JButton("Continue");
		btnContinue.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txtKey.setText("");
				txtValue.setText("");
			}
		});
		btnContinue.setBounds(188, 48, 89, 23);
		contentPane.add(btnContinue);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 85, 414, 165);
		contentPane.add(scrollPane);

		list = new JList();
		scrollPane.setViewportView(list);
	}
}
