package vn.t3h.chapter7;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JList;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class FrmBai4_Shop extends JFrame {

	private JPanel contentPane;
	private JTextField txtDogs;
	private JTextField txtCats;

	Bai4_PetManger<Bai4_Dog> managerDog = new Bai4_PetManger<Bai4_Dog>();
	Bai4_PetManger<Bai4_Cat> managerCat = new Bai4_PetManger<Bai4_Cat>();
	private JList list;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai4_Shop frame = new FrmBai4_Shop();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai4_Shop() {
		setTitle("Quản lý cửa hàng thú cưng");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 558, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblCcBnDog = new JLabel("Các bạn dog được nhập về");
		lblCcBnDog.setBounds(10, 11, 154, 14);
		contentPane.add(lblCcBnDog);

		txtDogs = new JTextField();
		txtDogs.setBounds(160, 8, 380, 20);
		contentPane.add(txtDogs);
		txtDogs.setColumns(10);

		txtCats = new JTextField();
		txtCats.setColumns(10);
		txtCats.setBounds(160, 36, 380, 20);
		contentPane.add(txtCats);

		JLabel lblCcBnCat = new JLabel("Các bạn cat được nhập về");
		lblCcBnCat.setBounds(10, 39, 154, 14);
		contentPane.add(lblCcBnCat);

		JButton btnThemMoi = new JButton("Thêm mới");
		btnThemMoi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				String dogs = txtDogs.getText();
				String cats = txtCats.getText();
				String dogNames[] = dogs.split(",");
				String catNames[] = cats.split(",");

				for (String dogname : dogNames) {
					managerDog.getPets().add(new Bai4_Dog(dogname));
				}

				for (String catname : catNames) {
					managerCat.getPets().add(new Bai4_Cat(catname));
				}

				DefaultListModel lst = new DefaultListModel();

				if (managerDog != null && !managerDog.getPets().isEmpty()) {
					for (int i = 0; i < managerDog.getPets().size(); i++) {
						lst.addElement(managerDog.getPets().get(i));
					}
				}
				if (managerCat != null && !managerCat.getPets().isEmpty()) {
					for (int i = 0; i < managerCat.getPets().size(); i++) {
						lst.addElement(managerCat.getPets().get(i));
					}
				}
				list.setModel(lst);
			}
		});
		btnThemMoi.setBounds(117, 73, 91, 23);
		contentPane.add(btnThemMoi);

		JButton btnTiepTuc = new JButton("Tiếp tục");
		btnTiepTuc.setBounds(241, 73, 91, 23);
		contentPane.add(btnTiepTuc);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 104, 530, 158);
		contentPane.add(scrollPane);

		list = new JList();
		scrollPane.setViewportView(list);
	}
}
