package vn.t3h.chapter7;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.awt.event.ActionEvent;

public class FrmBai1_ArrayList extends JFrame {

	private JPanel contentPane;
	private JComboBox cbbMang;
	private JTextArea taMang;
	private JTextArea taSapXep;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai1_ArrayList frame = new FrmBai1_ArrayList();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai1_ArrayList() {
		setTitle("In mảng chuỗi, số nguyên, số thực");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 367, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblLoiMng = new JLabel("Loại mảng");
		lblLoiMng.setBounds(10, 11, 69, 14);
		contentPane.add(lblLoiMng);

		cbbMang = new JComboBox();
		cbbMang.setModel(new DefaultComboBoxModel(new String[] { "Chuỗi", "Số nguyên", "Số thực" }));
		cbbMang.setBounds(89, 8, 248, 20);
		contentPane.add(cbbMang);

		JLabel lblNhpMngmi = new JLabel("Nhập mảng (mỗi phần tử cách nhau bởi khoảng trắng)");
		lblNhpMngmi.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNhpMngmi.setHorizontalAlignment(SwingConstants.CENTER);
		lblNhpMngmi.setBounds(10, 36, 327, 14);
		contentPane.add(lblNhpMngmi);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 61, 331, 59);
		contentPane.add(scrollPane);

		taMang = new JTextArea();
		scrollPane.setViewportView(taMang);

		JLabel lblMngSauKhi = new JLabel("Mảng sau khi sắp xếp");
		lblMngSauKhi.setHorizontalAlignment(SwingConstants.CENTER);
		lblMngSauKhi.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblMngSauKhi.setBounds(10, 131, 327, 14);
		contentPane.add(lblMngSauKhi);

		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(10, 156, 331, 65);
		contentPane.add(scrollPane_1);

		taSapXep = new JTextArea();
		scrollPane_1.setViewportView(taSapXep);

		JButton btnSpXpMng = new JButton("Sắp xếp mảng");
		btnSpXpMng.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				String[] mang = taMang.getText().split(" ");

				List lst = new ArrayList();

				if (cbbMang.getSelectedIndex() == 0) {
					lst.addAll(Arrays.asList(mang));
				} else if (cbbMang.getSelectedIndex() == 1) {
					for (String i : mang) {
						lst.add(Integer.parseInt(i));
					}
				} else {
					for (String i : mang) {
						lst.add(Double.parseDouble(i));
					}
				}
				
				Collections.sort(lst);
				
				taSapXep.setText(lst.toString());
			}
		});
		btnSpXpMng.setBounds(119, 227, 119, 23);
		contentPane.add(btnSpXpMng);
	}
}
