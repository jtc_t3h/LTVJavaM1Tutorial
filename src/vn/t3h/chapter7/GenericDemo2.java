package vn.t3h.chapter7;

public class GenericDemo2 {

	public static void main(String[] args) {

		String[] sArray = {"a", "b","c"};
		Integer[] iArray = {1,2,3};
		Double[] dArray = {1.1,2.2,3.3};
		
		// Duyet cac phan tu cua mang
		printArray(sArray);
		printArray(iArray);
		printArray(dArray);
		
//		for (String element: sArray) {
//			System.out.println(element);
//		}
//		
//		for (Integer element: iArray) {
//			System.out.println(element);
//		}
//		
//		for (Double element: dArray) {
//			System.out.println(element);
//		}
	}
	
	public static<E> void printArray(E[] arr) {
		for (E element: arr) {
			System.out.println(element);
		}
	}

}
