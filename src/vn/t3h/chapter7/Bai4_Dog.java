package vn.t3h.chapter7;

public class Bai4_Dog {

	private String name;

	public Bai4_Dog(String name){
	 this.name = name;
	 }

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return this.name;
	}

}
