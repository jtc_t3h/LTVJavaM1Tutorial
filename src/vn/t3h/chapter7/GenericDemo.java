package vn.t3h.chapter7;

public class GenericDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Integer [] iArrs = {1,2,4,5,6,7};
		Double[] dArrs = {1.1, 2.2, 3.3};
		String[] sArrs = {"a", "b", "c"};
		
		printArrs(iArrs);
		printArrs(dArrs);
		printArrs(sArrs);
	}
	
//	public static void printArrs(int [] arr) {
//		for (int e: arr) {
//			System.out.println(e);
//		}
//	}
//	
//	public static void printArrs(double [] arr) {
//		for (double e: arr) {
//			System.out.println(e);
//		}
//	}
//	
//	
//	public static void printArrs(String [] arr) {
//		for (String e: arr) {
//			System.out.println(e);
//		}
//	}
	
	// Lap trinh Generic
	public static<E> void printArrs(E [] arr) {
		for (E e: arr) {
			System.out.println(e);
		}
	}

}
