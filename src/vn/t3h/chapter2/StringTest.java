package vn.t3h.chapter2;

import java.util.StringTokenizer;

public class StringTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
//		String s1 = "hello world";
//		String s2 = new String("hello world");
//		String s3 = "hello world";
//		
//		System.out.println(s1 == s2); // false
//		System.out.println(s1 == s3); // false
//		System.out.println(s1.equals(s2)); // true
		
		
		String s4 = "hello";
		s4 = s4.concat(" world");
		
		System.out.println(s4); // 
		
		StringBuilder sb = new StringBuilder();
		
		
		StringTokenizer st = new StringTokenizer("Lap trinh java", " ");
		System.out.println("countTokens= " + st.countTokens());
		// duyet danh sach cac token
		while (st.hasMoreTokens()) {
			System.out.println("token = " + st.nextToken());
		}
		
		System.out.println(st.countTokens());
	}

}
