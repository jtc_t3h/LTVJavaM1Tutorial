package vn.t3h.chapter2;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;
import java.awt.Font;

public class FrmBai2_GiaiThua extends JFrame {

	private JPanel contentPane;
	private JTextField txtX;
	private JTextField txtKq;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai2_GiaiThua frame = new FrmBai2_GiaiThua();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai2_GiaiThua() {
		setTitle("T\u00EDnh giai th\u1EEBa");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 219, 152);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblTnhGiaiTha = new JLabel("T\u00EDnh giai th\u1EEBa c\u1EE7a x");
		lblTnhGiaiTha.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblTnhGiaiTha.setHorizontalAlignment(SwingConstants.CENTER);
		lblTnhGiaiTha.setBounds(10, 11, 184, 14);
		contentPane.add(lblTnhGiaiTha);
		
		JLabel lblNhpX = new JLabel("Nh\u1EADp x");
		lblNhpX.setBounds(10, 36, 46, 14);
		contentPane.add(lblNhpX);
		
		txtX = new JTextField();
		txtX.setBounds(66, 33, 128, 20);
		contentPane.add(txtX);
		txtX.setColumns(10);
		
		JLabel lblKtQu = new JLabel("K\u1EBFt qu\u1EA3");
		lblKtQu.setBounds(10, 60, 46, 14);
		contentPane.add(lblKtQu);
		
		txtKq = new JTextField();
		txtKq.setEditable(false);
		txtKq.setColumns(10);
		txtKq.setBounds(66, 57, 128, 20);
		contentPane.add(txtKq);
		
		JButton btnTnh = new JButton("T\u00EDnh");
		btnTnh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int x = Integer.parseInt(txtX.getText().trim());
				
				int kq = -1;
				if (x  == 0) {
					kq = 1;
				} else if (x > 0){
					kq = 1;
					for (int i = 1; i <= x; i++) {
						kq = kq * i;
					}
				} else {
					kq = -1;
				}
				
				txtKq.setText(String.valueOf(kq));
			}
		});
		btnTnh.setBounds(10, 88, 88, 23);
		contentPane.add(btnTnh);
		
		JButton btnNhapLai = new JButton("Nh\u1EADp l\u1EA1i");
		btnNhapLai.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txtX.setText("");
				txtKq.setText("");
			}
		});
		btnNhapLai.setBounds(106, 88, 88, 23);
		contentPane.add(btnNhapLai);
	}

}
