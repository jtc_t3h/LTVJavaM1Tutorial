package vn.t3h.chapter2;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.StringTokenizer;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;

public class FrmBai5_HoTen extends JFrame {

	private JPanel contentPane;
	private JTextField txtHovaten;
	private JTextField txtHo;
	private JTextField txtTendem;
	private JTextField txtTen;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai5_HoTen frame = new FrmBai5_HoTen();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai5_HoTen() {
		setTitle("T\u00E1ch h\u1ECD t\u00EAn");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 417);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblHVTn = new JLabel("H\u1ECD v\u00E0 t\u00EAn");
		lblHVTn.setBounds(10, 11, 71, 14);
		contentPane.add(lblHVTn);
		
		txtHovaten = new JTextField();
		txtHovaten.setBounds(91, 8, 341, 20);
		contentPane.add(txtHovaten);
		txtHovaten.setColumns(10);
		
		JLabel lblH = new JLabel("H\u1ECD");
		lblH.setBounds(10, 39, 71, 14);
		contentPane.add(lblH);
		
		txtHo = new JTextField();
		txtHo.setColumns(10);
		txtHo.setBounds(91, 36, 341, 20);
		contentPane.add(txtHo);
		
		JLabel lblTnm = new JLabel("T\u00EAn \u0111\u1EC7m");
		lblTnm.setBounds(10, 67, 71, 14);
		contentPane.add(lblTnm);
		
		txtTendem = new JTextField();
		txtTendem.setColumns(10);
		txtTendem.setBounds(91, 64, 341, 20);
		contentPane.add(txtTendem);
		
		JLabel lblTn = new JLabel("T\u00EAn");
		lblTn.setBounds(10, 95, 71, 14);
		contentPane.add(lblTn);
		
		txtTen = new JTextField();
		txtTen.setColumns(10);
		txtTen.setBounds(91, 92, 341, 20);
		contentPane.add(txtTen);
		
		JButton btnTachhoten = new JButton("T\u00E1ch h\u1ECD t\u00EAn");
		btnTachhoten.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String hovaTen = txtHovaten.getText().trim();
				
				StringTokenizer sb = new StringTokenizer(hovaTen);
				
				String ho = "";
				String ten_dem = "";
				String ten = "";
				int countToken = sb.countTokens();
				for (int i = 0; i < countToken; i++) {
					if (i == 0) {
						ho = sb.nextToken();
					} else if (i == countToken - 1) {
						ten = sb.nextToken();
					} else {
						ten_dem += sb.nextToken() + " ";
					}
					
				}
				txtHo.setText(ho);
				txtTendem.setText(ten_dem);
				txtTen.setText(ten);
			}
		});
		btnTachhoten.setBounds(170, 137, 109, 23);
		contentPane.add(btnTachhoten);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(FrmBai5_HoTen.class.getResource("/vn/t3h/resources/dau.jpg")));
		lblNewLabel.setBounds(56, 235, 99, 99);
		contentPane.add(lblNewLabel);
		
		JPanel panel = new JPanel();
		panel.setBounds(38, 195, 309, 170);
		contentPane.add(panel);
	}
}
