package vn.t3h.chapter2;

public class TestVariable {
	
	final static float PI = 3.14F; // instance variable -> static variable
	
	

	public static void main(String[] args) {
		
		int a;
//		System.out.println(a);
		
		int i = 300;
		byte b = 100;
		
		b = (byte)i;
		i = b;
		
		System.out.println(b);
		System.out.println(i);
	}
	
	public void aMethod(){
		System.out.println(PI);
		
	}

	
	public void abc(){
		System.out.println(PI);
	}
}
