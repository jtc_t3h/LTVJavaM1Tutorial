package vn.t3h.chapter2;

import java.io.File;

public class CharTest {

	public static void main(String[] args) {
		Character c1 = new Character('a');
		Character c2 = 'A';
		
		System.out.println(Character.toString(c1));
		
		File file = new File(".");
		System.out.println(file.getAbsolutePath());
	}
}
