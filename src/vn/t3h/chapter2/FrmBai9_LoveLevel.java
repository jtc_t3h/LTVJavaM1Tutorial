package vn.t3h.chapter2;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import javax.swing.ImageIcon;

public class FrmBai9_LoveLevel extends JFrame {

	private JPanel contentPane;
	private JTextField txtName1;
	private JTextField txtName2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai9_LoveLevel frame = new FrmBai9_LoveLevel();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai9_LoveLevel() {
		setTitle("Love Level");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 443, 295);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JLabel lblEnterNames = new JLabel("Enter Names");
		lblEnterNames.setBounds(15, 16, 89, 17);
		lblEnterNames.setFont(new Font("Tahoma", Font.BOLD, 14));
		
		txtName1 = new JTextField();
		txtName1.setBounds(15, 44, 117, 20);
		txtName1.setColumns(10);
		
		txtName2 = new JTextField();
		txtName2.setBounds(302, 44, 117, 20);
		txtName2.setColumns(10);
		
		JButton btnCompute = new JButton("Compute");
		btnCompute.setBounds(173, 220, 97, 25);
		btnCompute.setFont(new Font("Tahoma", Font.BOLD, 14));
		
		JLabel lblLoveLevel = new JLabel("");
		lblLoveLevel.setBounds(173, 160, 97, 31);
		lblLoveLevel.setHorizontalAlignment(SwingConstants.CENTER);
		lblLoveLevel.setFont(new Font("Tahoma", Font.BOLD, 14));
		contentPane.setLayout(null);
		contentPane.add(txtName1);
		contentPane.add(lblEnterNames);
		contentPane.add(lblLoveLevel);
		contentPane.add(btnCompute);
		contentPane.add(txtName2);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(FrmBai9_LoveLevel.class.getResource("/vn/t3h/resources/love.jpg")));
		lblNewLabel.setBounds(0, 0, 434, 261);
		contentPane.add(lblNewLabel);
	}

}
