package vn.t3h.chapter2;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;

public class FrmBai6_DayOfMonth extends JFrame {

	private JPanel contentPane;
	private JTextField txtThang;
	private JTextField txtNam;
	private JTextField txtKQ;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai6_DayOfMonth frame = new FrmBai6_DayOfMonth();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai6_DayOfMonth() {
		setTitle("Tính số ngày trong tháng");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 364, 187);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNh = new JLabel("Nhập tháng");
		lblNh.setBounds(12, 13, 79, 16);
		contentPane.add(lblNh);
		
		txtThang = new JTextField();
		txtThang.setBounds(105, 10, 102, 22);
		contentPane.add(txtThang);
		txtThang.setColumns(10);
		
		JLabel lblNhpNm = new JLabel("Nhập năm");
		lblNhpNm.setBounds(12, 37, 79, 16);
		contentPane.add(lblNhpNm);
		
		txtNam = new JTextField();
		txtNam.setColumns(10);
		txtNam.setBounds(105, 34, 102, 22);
		contentPane.add(txtNam);
		
		JLabel lblKtQu = new JLabel("Kết quả");
		lblKtQu.setBounds(12, 66, 79, 16);
		contentPane.add(lblKtQu);
		
		txtKQ = new JTextField();
		txtKQ.setColumns(10);
		txtKQ.setBounds(105, 63, 102, 22);
		contentPane.add(txtKQ);
		
		JButton btnTinh = new JButton("Tính");
		btnTinh.setBounds(110, 102, 97, 25);
		contentPane.add(btnTinh);
	}

}
