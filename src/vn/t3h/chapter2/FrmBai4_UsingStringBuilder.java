package vn.t3h.chapter2;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import java.awt.Component;
import javax.swing.JButton;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class FrmBai4_UsingStringBuilder extends JFrame {

	private JPanel contentPane;
	private JTextField txtSb;
	private JTextField txtSb1;
	private JTextField txtSb2;
	private JTextField txtVitri;
	private JTextField txtBatdau;
	private JLabel lblVTrCui;
	private JTextField txtKetthuc;
	private JTextArea taKetqua;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai4_UsingStringBuilder frame = new FrmBai4_UsingStringBuilder();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai4_UsingStringBuilder() {
		setTitle("X\u1EED l\u00FD chu\u1ED7i StringBuilder");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 438);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JLabel lblSb = new JLabel("Chu\u1ED7i sb");
		
		txtSb = new JTextField();
		txtSb.setColumns(10);
		
		txtSb1 = new JTextField();
		txtSb1.setColumns(10);
		
		JLabel lblSb1 = new JLabel("Chu\u1ED7i sb1");
		
		txtSb2 = new JTextField();
		txtSb2.setColumns(10);
		
		JLabel lblChuiSb = new JLabel("Chu\u1ED7i sb2");
		
		txtVitri = new JTextField();
		txtVitri.setColumns(10);
		
		JLabel lblVTrChn = new JLabel("V\u1ECB tr\u00ED ch\u00E8n");
		
		txtBatdau = new JTextField();
		txtBatdau.setColumns(10);
		
		JLabel lblVTru = new JLabel("V\u1ECB tr\u00ED \u0111\u1EA7u");
		
		lblVTrCui = new JLabel("V\u1ECB tr\u00ED cu\u1ED1i");
		
		txtKetthuc = new JTextField();
		txtKetthuc.setColumns(10);
		
		JButton btnXuly = new JButton("X\u1EED l\u00FD chu\u1ED7i");
		btnXuly.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnXuly_actionPerformed(arg0);
			}
		});
		
		JLabel lblKtQu = new JLabel("K\u1EBFt qu\u1EA3");
		
		JScrollPane scrollPane = new JScrollPane();
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(10)
					.addComponent(lblSb1, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(338, Short.MAX_VALUE))
				.addGroup(Alignment.TRAILING, gl_contentPane.createSequentialGroup()
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addComponent(lblSb, GroupLayout.DEFAULT_SIZE, 76, Short.MAX_VALUE)
					.addGap(338))
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(10)
					.addComponent(lblChuiSb, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(338, Short.MAX_VALUE))
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(10)
					.addComponent(lblVTrChn, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(338, Short.MAX_VALUE))
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(104)
					.addComponent(txtVitri, GroupLayout.PREFERRED_SIZE, 300, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(104)
					.addComponent(txtSb2, GroupLayout.PREFERRED_SIZE, 300, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(104)
					.addComponent(txtSb1, GroupLayout.PREFERRED_SIZE, 300, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(104)
					.addComponent(txtSb, GroupLayout.PREFERRED_SIZE, 310, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(lblVTru, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblVTrCui, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblKtQu, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
						.addComponent(scrollPane)
						.addComponent(btnXuly)
						.addComponent(txtKetthuc, GroupLayout.DEFAULT_SIZE, 310, Short.MAX_VALUE)
						.addComponent(txtBatdau, GroupLayout.DEFAULT_SIZE, 300, Short.MAX_VALUE))
					.addContainerGap())
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblSb)
						.addComponent(txtSb, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(txtSb1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(3)
							.addComponent(lblSb1)))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(txtSb2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(3)
							.addComponent(lblChuiSb)))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(txtVitri, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(3)
							.addComponent(lblVTrChn)))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
						.addComponent(lblVTru)
						.addComponent(txtBatdau, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(txtKetthuc, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(9)
							.addComponent(lblVTrCui)))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(btnXuly)
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(lblKtQu)
							.addGap(162))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 165, Short.MAX_VALUE)
							.addContainerGap())))
		);
		gl_contentPane.linkSize(SwingConstants.HORIZONTAL, new Component[] {lblVTru, lblVTrCui, lblSb, lblSb1, lblChuiSb, lblVTrChn});
		gl_contentPane.linkSize(SwingConstants.HORIZONTAL, new Component[] {txtKetthuc, txtSb, txtSb1, txtSb2, txtVitri, txtBatdau});
		
		taKetqua = new JTextArea();
		scrollPane.setViewportView(taKetqua);
		contentPane.setLayout(gl_contentPane);
	}

	protected void btnXuly_actionPerformed(ActionEvent arg0) {
		String sb = txtSb.getText();
		String sb1 = txtSb1.getText();
		String sb2 = txtSb2.getText();

		int viTri = Integer.parseInt(txtVitri.getText());
		int batDau = Integer.parseInt(txtBatdau.getText());
		int ketThuc = Integer.parseInt(txtKetthuc.getText());

		 String ketQua = "Kết quả:";

		 StringBuilder str = new StringBuilder(200);
		 str.append(sb);
		 
		 ketQua += "\n Chuỗi sb: " + str;
		 ketQua += "\n Chiều dài: " + str.length();
		 str.append(sb1);
		 ketQua += "\n Chuỗi sb sau khi nối sb1: " + str;
		 ketQua += "\n Chiều dài: " + str.length();
		 str.insert(viTri, sb2);
		 ketQua += "\n Sau khi chèn sb2 vào vị trí " + viTri + ": \n" + str;
		 str.delete(batDau, ketThuc);
		 ketQua += "\n Sau khi xóa: \n" + str;
		 str.reverse();
		 ketQua += "\n Sau khi đảo ngược: \n" + str;
		 
		 taKetqua.setText(ketQua);
	}
}
