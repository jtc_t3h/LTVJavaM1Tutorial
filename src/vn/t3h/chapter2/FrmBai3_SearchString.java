package vn.t3h.chapter2;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class FrmBai3_SearchString extends JFrame {

	private JPanel contentPane;
	private JTextField txtChuoi1;
	private JTextField txtChuoi2;
	private JTextField txtKetQua;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai3_SearchString frame = new FrmBai3_SearchString();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai3_SearchString() {
		setTitle("Tìm kiếm chuỗi");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 192);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNhpChui = new JLabel("Nhập chuỗi 1");
		lblNhpChui.setBounds(12, 13, 90, 16);
		contentPane.add(lblNhpChui);
		
		txtChuoi1 = new JTextField();
		txtChuoi1.setBounds(114, 10, 306, 22);
		contentPane.add(txtChuoi1);
		txtChuoi1.setColumns(10);
		
		JLabel lblNhpChui_1 = new JLabel("Nhập chuỗi 2");
		lblNhpChui_1.setBounds(12, 45, 90, 16);
		contentPane.add(lblNhpChui_1);
		
		txtChuoi2 = new JTextField();
		txtChuoi2.setColumns(10);
		txtChuoi2.setBounds(114, 42, 306, 22);
		contentPane.add(txtChuoi2);
		
		JLabel lblKtQu = new JLabel("Kết quả");
		lblKtQu.setBounds(12, 77, 90, 16);
		contentPane.add(lblKtQu);
		
		txtKetQua = new JTextField();
		txtKetQua.setColumns(10);
		txtKetQua.setBounds(114, 74, 306, 22);
		contentPane.add(txtKetQua);
		
		JButton btnTmKim = new JButton("Tìm kiếm");
		btnTmKim.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String chuoi1 = txtChuoi1.getText().trim();
				String chuoi2 = txtChuoi2.getText().trim();
				
				if (chuoi1.contains(chuoi2)) {
					txtKetQua.setText("Yes");
				} else {
					txtKetQua.setText("No");
				}
				
			}
		});
		btnTmKim.setBounds(160, 115, 97, 25);
		contentPane.add(btnTmKim);
	}

}
