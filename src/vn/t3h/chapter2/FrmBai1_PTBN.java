package vn.t3h.chapter2;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.Font;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class FrmBai1_PTBN extends JFrame {

	private JPanel contentPane;
	private JTextField txtA;
	private JLabel lblNewLabel;
	private JTextField txtB;
	private JTextField txtKq;
	private JButton btnNhapLai;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai1_PTBN frame = new FrmBai1_PTBN();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai1_PTBN() {
		setTitle("Gi\u1EA3i ph\u01B0\u01A1ng tr\u00ECnh b\u1EADc I");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 299, 193);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		txtA = new JTextField();
		txtA.setBounds(101, 33, 149, 20);
		contentPane.add(txtA);
		txtA.setColumns(10);
		
		lblNewLabel = new JLabel("Nh\u1EADp a");
		lblNewLabel.setBounds(21, 36, 46, 14);
		contentPane.add(lblNewLabel);
		
		JLabel lblGiiPhngTrnh = new JLabel("Gi\u1EA3i ph\u01B0\u01A1ng tr\u00ECnh ax + b = 0");
		lblGiiPhngTrnh.setHorizontalAlignment(SwingConstants.CENTER);
		lblGiiPhngTrnh.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblGiiPhngTrnh.setBounds(21, 11, 260, 14);
		contentPane.add(lblGiiPhngTrnh);
		
		JLabel t = new JLabel("Nh\u1EADp b");
		t.setBounds(21, 67, 46, 14);
		contentPane.add(t);
		
		txtB = new JTextField();
		txtB.setColumns(10);
		txtB.setBounds(101, 64, 149, 20);
		contentPane.add(txtB);
		
		JLabel lblKtQu = new JLabel("K\u1EBFt qu\u1EA3");
		lblKtQu.setBounds(21, 94, 46, 14);
		contentPane.add(lblKtQu);
		
		txtKq = new JTextField();
		txtKq.setEditable(false);
		txtKq.setColumns(10);
		txtKq.setBounds(101, 91, 149, 20);
		contentPane.add(txtKq);
		
		JButton btnNewButton = new JButton("T\u00EDnh");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				double a = Double.valueOf(txtA.getText().trim());
				double b = Double.valueOf(txtB.getText().trim());
				
				if (a == 0 && b == 0) {
					txtKq.setText("Vô số nghiệm");
				} else if (a == 0 && b != 0) {
					txtKq.setText("Vô nghiệm");
				} else {
					txtKq.setText(String.valueOf(-b/a));
				}
			}
		});
		btnNewButton.setBounds(43, 122, 91, 23);
		contentPane.add(btnNewButton);
		
		btnNhapLai = new JButton("Nh\u1EADp l\u1EA1i");
		btnNhapLai.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				txtA.setText("");
				txtB.setText("");
				txtKq.setText("");
			}
		});
		btnNhapLai.setBounds(159, 122, 91, 23);
		contentPane.add(btnNhapLai);
	}
}
