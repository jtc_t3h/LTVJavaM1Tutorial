package vn.t3h.chapter2;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import javax.swing.SwingConstants;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class FrmBai7_Zodiac extends JFrame {

	private JPanel contentPane;
	private JTextField txtYear;
	private JLabel lblCan;
	private JLabel lblChi;
	private JLabel lblImage;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai7_Zodiac frame = new FrmBai7_Zodiac();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai7_Zodiac() {
		setTitle("Zodiac");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 378, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JLabel lblYearOfBirthday = new JLabel("Year of Birthday");
		
		txtYear = new JTextField();
		txtYear.setColumns(10);
		
		JButton btnOk = new JButton("OK");
		btnOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnOk_actionPerformed(e);
			}
		});
		
	 	lblImage = new JLabel("");
		lblImage.setIcon(new ImageIcon(FrmBai7_Zodiac.class.getResource("/vn/t3h/resources/dan.jpg")));
		
		lblCan = new JLabel("");
		lblCan.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblCan.setForeground(Color.RED);
		lblCan.setHorizontalAlignment(SwingConstants.RIGHT);
		
		lblChi = new JLabel("");
		lblChi.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblChi.setForeground(Color.RED);
		lblChi.setHorizontalAlignment(SwingConstants.LEFT);
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(58)
							.addComponent(lblYearOfBirthday)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(txtYear, GroupLayout.PREFERRED_SIZE, 96, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addContainerGap()
							.addComponent(btnOk, GroupLayout.DEFAULT_SIZE, 332, Short.MAX_VALUE))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(113)
							.addComponent(lblImage))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(101)
							.addComponent(lblCan, GroupLayout.PREFERRED_SIZE, 61, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addComponent(lblChi, GroupLayout.PREFERRED_SIZE, 63, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap())
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblYearOfBirthday)
						.addComponent(txtYear, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(btnOk)
					.addGap(18)
					.addComponent(lblImage, GroupLayout.PREFERRED_SIZE, 124, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(lblCan, GroupLayout.DEFAULT_SIZE, 22, Short.MAX_VALUE)
						.addComponent(lblChi, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE))
					.addContainerGap())
		);
		contentPane.setLayout(gl_contentPane);
	}

	protected void btnOk_actionPerformed(ActionEvent e) {
		displayCan();
		displayChi();
	}

	private void displayChi() {
		int year = Integer.parseInt(txtYear.getText().trim());
		switch (year % 12) {
		case 0:
			lblChi.setText("Thân");
			lblImage.setIcon(new ImageIcon(FrmBai7_Zodiac.class.getResource("/vn/t3h/resources/than.jpg")));
			break;
		case 1:
			lblChi.setText("Dậu");
			lblImage.setIcon(new ImageIcon(FrmBai7_Zodiac.class.getResource("/vn/t3h/resources/dau.jpg")));
			break;
		case 2:
			lblChi.setText("Tuất");
			lblImage.setIcon(new ImageIcon(FrmBai7_Zodiac.class.getResource("/vn/t3h/resources/tuat.jpg")));
			break;
		case 3:
			lblChi.setText("Hợi");
			lblImage.setIcon(new ImageIcon(FrmBai7_Zodiac.class.getResource("/vn/t3h/resources/hoi.jpg")));
			break;
		case 4:
			lblChi.setText("Tý");
			lblImage.setIcon(new ImageIcon(FrmBai7_Zodiac.class.getResource("/vn/t3h/resources/ty.jpg")));
			break;
		case 5:
			lblChi.setText("Sửu");
			lblImage.setIcon(new ImageIcon(FrmBai7_Zodiac.class.getResource("/vn/t3h/resources/suu.jpg")));
			break;
		case 6:
			lblChi.setText("Dần");
			lblImage.setIcon(new ImageIcon(FrmBai7_Zodiac.class.getResource("/vn/t3h/resources/dan.jpg")));
			break;
		case 7:
			lblChi.setText("Mão");
			lblImage.setIcon(new ImageIcon(FrmBai7_Zodiac.class.getResource("/vn/t3h/resources/meo.jpg")));
			break;
		case 8:
			lblChi.setText("Thìn");
			lblImage.setIcon(new ImageIcon(FrmBai7_Zodiac.class.getResource("/vn/t3h/resources/thin.jpg")));
			break;
		case 9:
			lblChi.setText("Tỵ");
			lblImage.setIcon(new ImageIcon(FrmBai7_Zodiac.class.getResource("/vn/t3h/resources/ti.jpg")));
			break;
		case 10:
			lblChi.setText("Ngọ");
			lblImage.setIcon(new ImageIcon(FrmBai7_Zodiac.class.getResource("/vn/t3h/resources/Ngo.jpg")));
			break;
		case 11:
			lblChi.setText("Mùi");
			lblImage.setIcon(new ImageIcon(FrmBai7_Zodiac.class.getResource("/vn/t3h/resources/Mui.jpg")));
			break;
		}
	}

	private void displayCan() {
		int year = Integer.parseInt(txtYear.getText().trim());
		switch (year % 10) {
		case 0:
			lblCan.setText("Canh");
			break;
		case 1:
			lblCan.setText("Tân");
			break;
		case 2:
			lblCan.setText("Nhâm");
			break;
		case 3:
			lblCan.setText("Quý");
			break;
		case 4:
			lblCan.setText("Giáp");
			break;
		case 5:
			lblCan.setText("Ất");
			break;
		case 6:
			lblCan.setText("Bính");
			break;
		case 7:
			lblCan.setText("Đinh");
			break;
		case 8:
			lblCan.setText("Mậu");
			break;
		case 9:
			lblCan.setText("Kỷ");
			break;
		}
	}

}
