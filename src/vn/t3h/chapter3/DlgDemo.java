package vn.t3h.chapter3;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.ImageIcon;
import java.awt.Window.Type;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class DlgDemo extends JDialog {

	private final JPanel contentPanel = new JPanel(); // field = truong du lieu
	private JButton okButton;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			DlgDemo dialog = new DlgDemo();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public DlgDemo() {
		setType(Type.POPUP);
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		
		
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		{
			JLabel lblNewLabel = new JLabel("");
			lblNewLabel.setBounds(10, 11, 150, 139);
			lblNewLabel.setIcon(new ImageIcon(DlgDemo.class.getResource("/vn/t3h/resources/Default-avatar.png")));
			contentPanel.add(lblNewLabel);
		}
		{
			JButton btnNewButton = new JButton("");
			btnNewButton.setIcon(new ImageIcon(DlgDemo.class.getResource("/vn/t3h/resources/Cart-icon.png")));
			btnNewButton.setBounds(224, 21, 160, 23);
			contentPanel.add(btnNewButton);
		}
		{
			textField = new JTextField();
			textField.setBounds(191, 82, 215, 20);
			contentPanel.add(textField);
			textField.setColumns(10);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				okButton = new JButton("OK"); // local
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						System.out.println(textField.toString());
					}
				});
				okButton.setToolTipText("Ban dang re chuot vao button");
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseEntered(MouseEvent arg0) {
						JOptionPane.showMessageDialog(null, "Ban dang re chuot vao thanh phan dieu khien");
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
			
			
		}
	}

}
