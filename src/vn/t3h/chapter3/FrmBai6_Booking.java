package vn.t3h.chapter3;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.io.File;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class FrmBai6_Booking extends JFrame {

	private JPanel contentPane;
	private JTextField txtSeat;
	private JTextField txtName;
	private JComboBox cbbMovie;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai6_Booking frame = new FrmBai6_Booking();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai6_Booking() {
		setTitle("Ticket register form");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 285);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblSeat = new JLabel("Seat(s)");
		lblSeat.setBounds(10, 11, 46, 14);
		contentPane.add(lblSeat);
		
		txtSeat = new JTextField();
		txtSeat.setBounds(93, 8, 86, 20);
		contentPane.add(txtSeat);
		txtSeat.setColumns(10);
		
		JLabel lblName = new JLabel("Name");
		lblName.setBounds(10, 36, 46, 14);
		contentPane.add(lblName);
		
		txtName = new JTextField();
		txtName.setBounds(93, 33, 331, 20);
		contentPane.add(txtName);
		txtName.setColumns(10);
		
		JLabel lblMovies = new JLabel("Movies");
		lblMovies.setBounds(10, 64, 46, 14);
		contentPane.add(lblMovies);
		
		// init data for cbbMovie
		
//		cbbMovie = new JComboBox();
//		cbbMovie.setBounds(93, 61, 331, 20);
//		contentPane.add(cbbMovie);
		
		// Tao ra danh sach film --> bien data là mảng có 6 phần tử (các phan tu có kiểu String)
		String[] data = getFilmList();
		
		// Tao ra mang (chỉ số) --> mảng có 6 phần và giá trị lần lượt 0, 1, 2, 3, 4, 5
		Integer[] idx = new Integer[data.length];
		for (int i = 0; i < data.length; i++){
			idx[i] = i;
		}
		
		cbbMovie = new JComboBox(idx);
		cbbMovie.setRenderer(new ImageRender(data));
		cbbMovie.setBackground(new Color(255, 255, 255));
		cbbMovie.setBounds(93, 64, 331, 134);
		contentPane.add(cbbMovie);
		
//		loadDataOfCbbMovie();
		
		JButton btnBook = new JButton("Booking");
		btnBook.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			}
		});
		btnBook.setBounds(167, 216, 89, 23);
		contentPane.add(btnBook);
	}
	
	private void loadDataOfCbbMovie() {
		String[] data = getFilmList();
		Integer[] idx = new Integer[data.length];
		for (int i = 0; i < data.length; i++){
			idx[i] = i;
		}
		
		cbbMovie = new JComboBox(idx);
		cbbMovie.setBackground(new Color(255, 255, 255));
		cbbMovie.setBounds(93, 61, 331, 20);
		
	}

	/**
	 * 
	 * @return
	 * 		Một mảng gồm danh sách file được đọc từ file FilmList.txt
	 */
	private String[] getFilmList() {
		File myFile = new File("./src/vn/t3h/chapter3/FilmList.txt");
		String s = null;
		
		try {
			s = ReadOrWriteFile.readTXT(myFile);
			String temps[] = s.split("\n");
			
			return temps;
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "File not found!", "Error!", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
		return null;	
	}
}
