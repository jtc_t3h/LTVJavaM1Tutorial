package vn.t3h.chapter3;

import java.awt.Panel;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class CreateFrame extends JFrame {

	public static void main(String[] args) {
		CreateFrame frm = new CreateFrame();
		frm.setBounds(50, 50, 400, 400);
		frm.setVisible(true);
		
		JPanel pn = new JPanel();
		JLabel lbl = new JLabel("Hello");
		
		System.out.println(frm.toString());
		
		pn.add(lbl);
		frm.add(pn);
	}
}
