package vn.t3h.chapter3;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class FrmBai7_ChooseColor extends JFrame {

	private JPanel contentPane;
	private JButton btnNewButton;
	private JPanel pnColor;
	private JLabel lblImage;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai7_ChooseColor frame = new FrmBai7_ChooseColor();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai7_ChooseColor() {
		setTitle("Choose your color");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 406, 303);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		pnColor = new JPanel();
		pnColor.setBounds(0, 0, 390, 261);
		contentPane.add(pnColor);
		pnColor.setLayout(null);
		
		btnNewButton = new JButton("Choose Color");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Color c = new Color(0);
				c = JColorChooser.showDialog(null, "Pick a color", c);
				
				btnNewButton.setForeground(c);
				pnColor.setBackground(c);
			}
		});
		btnNewButton.setIcon(new ImageIcon(FrmBai7_ChooseColor.class.getResource("/vn/t3h/resources/color-chooser-icon.png")));
		btnNewButton.setBounds(132, 0, 140, 23);
		pnColor.add(btnNewButton);
		
		lblImage = new JLabel("");
		lblImage.setIcon(new ImageIcon(FrmBai7_ChooseColor.class.getResource("/vn/t3h/resources/flower.png")));
		lblImage.setBounds(0, 0, 390, 261);
		pnColor.add(lblImage);
	}

}
