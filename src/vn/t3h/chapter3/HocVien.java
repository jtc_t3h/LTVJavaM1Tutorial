package vn.t3h.chapter3;

public class HocVien {

	private String name;

	public HocVien(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return name;
	}
	
	
}
