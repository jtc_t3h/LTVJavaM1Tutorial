package vn.t3h.chapter3;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JSeparator;

public class FrmBai3 extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai3 frame = new FrmBai3();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai3() {
		setTitle("Th\u00F4ng tin li\u00EAn h\u1EC7");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 413, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblH = new JLabel("H\u1ECD t\u00EAn");
		lblH.setBounds(10, 11, 84, 14);
		contentPane.add(lblH);
		
		textField = new JTextField();
		textField.setBounds(104, 8, 285, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JButton btnTmKim = new JButton("T\u00ECm ki\u1EBFm");
		btnTmKim.setBounds(104, 39, 89, 23);
		contentPane.add(btnTmKim);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(10, 77, 377, 2);
		contentPane.add(separator);
		
		JLabel label = new JLabel("H\u1ECD t\u00EAn");
		label.setBounds(10, 108, 84, 14);
		contentPane.add(label);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(104, 105, 285, 20);
		contentPane.add(textField_1);
		
		JLabel lbltd = new JLabel("\u0110TD\u0110");
		lbltd.setBounds(10, 136, 84, 14);
		contentPane.add(lbltd);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(104, 133, 145, 20);
		contentPane.add(textField_2);
		
		JLabel lblHnhnh = new JLabel("H\u00ECnh \u1EA3nh");
		lblHnhnh.setBounds(10, 164, 84, 14);
		contentPane.add(lblHnhnh);
		
		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(104, 161, 242, 20);
		contentPane.add(textField_3);
		
		JButton button = new JButton("...");
		button.setBounds(352, 160, 35, 23);
		contentPane.add(button);
		
		JButton btnCpNht = new JButton("C\u1EADp nh\u1EADt");
		btnCpNht.setBounds(104, 196, 89, 23);
		contentPane.add(btnCpNht);
	}
}
