package vn.t3h.chapter3;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;

public class FrmBai2 extends JFrame {
	private JTextField txtHoTen;
	private JTextField txtDiDong;
	private JTextField txtHinhAnh;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai2 frame = new FrmBai2();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai2() {
		setTitle("Th\u00EAm m\u1EDBi li\u00EAn h\u1EC7");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 506, 204);
		getContentPane().setLayout(null);
		
		JLabel label = new JLabel("H\u1ECD t\u00EAn");
		label.setBounds(21, 14, 73, 14);
		getContentPane().add(label);
		
		txtHoTen = new JTextField();
		txtHoTen.setColumns(10);
		txtHoTen.setBounds(104, 11, 252, 20);
		getContentPane().add(txtHoTen);
		
		JLabel label_1 = new JLabel("\u0110TD\u0110");
		label_1.setBounds(21, 42, 73, 14);
		getContentPane().add(label_1);
		
		txtDiDong = new JTextField();
		txtDiDong.setColumns(10);
		txtDiDong.setBounds(103, 39, 156, 20);
		getContentPane().add(txtDiDong);
		
		txtHinhAnh = new JTextField();
		txtHinhAnh.setColumns(10);
		txtHinhAnh.setBounds(103, 67, 193, 20);
		getContentPane().add(txtHinhAnh);
		
		JLabel label_2 = new JLabel("H\u00ECnh \u1EA3nh");
		label_2.setBounds(21, 70, 73, 14);
		getContentPane().add(label_2);
		
		JButton btnThmMi = new JButton("Th\u00EAm m\u1EDBi");
		btnThmMi.setBounds(202, 128, 91, 23);
		getContentPane().add(btnThmMi);
		
		JLabel lblHinhAnh = new JLabel("");
		lblHinhAnh.setBounds(366, 11, 118, 136);
		getContentPane().add(lblHinhAnh);
		
		JButton btnNewButton = new JButton("...");
		btnNewButton.setBounds(309, 66, 47, 23);
		getContentPane().add(btnNewButton);
	}

}
