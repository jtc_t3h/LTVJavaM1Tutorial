package vn.t3h.chapter3;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFileChooser;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.border.TitledBorder;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.InputEvent;

public class FrmDemo extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private ButtonGroup genderButtonGroup = new ButtonGroup();
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmDemo frame = new FrmDemo();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmDemo() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 436);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnFile = new JMenu("File");
		mnFile.setMnemonic('F');
		menuBar.add(mnFile);
		
		JMenuItem mntmOpen = new JMenuItem("Open");
		mntmOpen.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_MASK));
		mnFile.add(mntmOpen);
		
		JMenuItem mntmSaveAs = new JMenuItem("Save As");
		mnFile.add(mntmSaveAs);
		
		JMenu mnNewMenu = new JMenu("New menu");
		mnFile.add(mnNewMenu);
		
		JMenuItem mntmNewMenuItem = new JMenuItem("New menu item");
		mnNewMenu.add(mntmNewMenuItem);
		
		JMenuItem mntmNewMenuItem_1 = new JMenuItem("New menu item");
		mnNewMenu.add(mntmNewMenuItem_1);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnOpenDialog = new JButton("Open Dialog");
		btnOpenDialog.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				DlgDemo dlg = new DlgDemo();
				dlg.setVisible(true);
			}
		});
		btnOpenDialog.setBounds(10, 123, 162, 23);
		contentPane.add(btnOpenDialog);
		
		textField = new JTextField();
		textField.setBounds(0, 30, 279, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JButton btnBrowser = new JButton("Browser");
		btnBrowser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				JFileChooser jfc = new JFileChooser();
				jfc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
				jfc.showOpenDialog(null);
				
				textField.setText(jfc.getSelectedFile().getAbsolutePath());
			}
		});
		btnBrowser.setBounds(289, 29, 91, 23);
		contentPane.add(btnBrowser);
		
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "Gioi tinh", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.setBounds(0, 61, 126, 51);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JRadioButton rdbtnNam = new JRadioButton("Nam");
		rdbtnNam.setBounds(6, 13, 47, 23);
		panel.add(rdbtnNam);
		rdbtnNam.setSelected(true);
		genderButtonGroup.add(rdbtnNam);
		
		JRadioButton rdbtnNu = new JRadioButton("Nu");
		rdbtnNu.setBounds(55, 7, 48, 34);
		panel.add(rdbtnNu);
		rdbtnNu.setSelected(true);
		genderButtonGroup.add(rdbtnNu);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new HocVien[] {new HocVien("Nguyen Van A"), new HocVien("Nguyen Van B")}));
		comboBox.setBounds(248, 90, 184, 22);
		contentPane.add(comboBox);
		
		JLabel lblDsHocVien = new JLabel("Ds hoc vien");
		lblDsHocVien.setBounds(147, 94, 91, 14);
		contentPane.add(lblDsHocVien);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(21, 157, 411, 241);
		contentPane.add(scrollPane);
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null},
			},
			new String[] {
				"STT", "ID", "Name", "New column"
			}
		));
		table.getColumnModel().getColumn(1).setPreferredWidth(168);
		table.getColumnModel().getColumn(2).setPreferredWidth(133);
		table.getColumnModel().getColumn(3).setPreferredWidth(197);
		scrollPane.setViewportView(table);
		
		JButton btnNewButton = new JButton("New button");
		btnNewButton.addActionListener(new MyActionListener());
		btnNewButton.setBounds(201, 123, 91, 23);
		contentPane.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("New button");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JOptionPane.showMessageDialog(null, "Ban dang click vao button nay");
			}
		});
		btnNewButton.addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub
				if (e.getKeyCode() == 13) {
					
				}
				
			}
		});
		btnNewButton_1.setBounds(304, 123, 91, 23);
		contentPane.add(btnNewButton_1);
	}
}
