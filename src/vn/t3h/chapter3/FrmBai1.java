package vn.t3h.chapter3;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JSeparator;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;

public class FrmBai1 extends JFrame {

	private JPanel contentPane;
	private JTextField txtHoTen;
	private JTextField txtDiDong;
	private JTextField txtHinhAnh;
	private JLabel lblHoTen;
	private JLabel lblDiDong;
	private JLabel lblHinhAnh;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai1 frame = new FrmBai1();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai1() {
		setTitle("Th\u00F4ng tin li\u00EAn h\u1EC7");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 437, 468);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblHTn = new JLabel("H\u1ECD t\u00EAn");
		lblHTn.setBounds(15, 20, 73, 14);
		contentPane.add(lblHTn);
		
		txtHoTen = new JTextField();
		txtHoTen.setBounds(98, 17, 315, 20);
		contentPane.add(txtHoTen);
		txtHoTen.setColumns(10);
		
		JLabel lbltd = new JLabel("\u0110TD\u0110");
		lbltd.setBounds(15, 48, 73, 14);
		contentPane.add(lbltd);
		
		txtDiDong = new JTextField();
		txtDiDong.setColumns(10);
		txtDiDong.setBounds(97, 45, 156, 20);
		contentPane.add(txtDiDong);
		
		JLabel lblHnhnh = new JLabel("H\u00ECnh \u1EA3nh");
		lblHnhnh.setBounds(15, 76, 73, 14);
		contentPane.add(lblHnhnh);
		
		txtHinhAnh = new JTextField();
		txtHinhAnh.setColumns(10);
		txtHinhAnh.setBounds(97, 73, 316, 20);
		contentPane.add(txtHinhAnh);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(15, 111, 398, 2);
		contentPane.add(separator);
		
		JLabel label_2 = new JLabel("H\u1ECD t\u00EAn");
		label_2.setBounds(15, 173, 73, 14);
		contentPane.add(label_2);
		
		JLabel lbltd_1 = new JLabel("\u0110TD\u0110");
		lbltd_1.setBounds(15, 201, 73, 14);
		contentPane.add(lbltd_1);
		
		JLabel lblHnhnh_1 = new JLabel("H\u00ECnh \u1EA3nh");
		lblHnhnh_1.setBounds(15, 229, 73, 14);
		contentPane.add(lblHnhnh_1);
		
		lblHoTen = new JLabel("");
		lblHoTen.setBounds(98, 173, 315, 14);
		contentPane.add(lblHoTen);
		
		lblDiDong = new JLabel("");
		lblDiDong.setBounds(98, 201, 315, 14);
		contentPane.add(lblDiDong);
		
		lblHinhAnh = new JLabel("");
		lblHinhAnh.setHorizontalAlignment(SwingConstants.LEFT);
		lblHinhAnh.setVerticalAlignment(SwingConstants.TOP);
		lblHinhAnh.setBounds(98, 229, 191, 201);
		contentPane.add(lblHinhAnh);
		
		JButton btnHienThi = new JButton("Hi\u1EC3n Th\u1ECB");
		btnHienThi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String hoTen = txtHoTen.getText().trim();
				String diDong = txtDiDong.getText().trim();
				String hinhAnh = txtHinhAnh.getText().trim();
				
				lblHoTen.setText(hoTen);
				lblDiDong.setText(diDong);
				lblHinhAnh.setIcon(new ImageIcon(hinhAnh));
			}
		});
		btnHienThi.setBounds(100, 124, 91, 23);
		contentPane.add(btnHienThi);
	}
}
