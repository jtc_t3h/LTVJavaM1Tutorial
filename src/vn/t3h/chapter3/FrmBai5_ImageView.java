package vn.t3h.chapter3;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class FrmBai5_ImageView extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai5_ImageView frame = new FrmBai5_ImageView();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai5_ImageView() {
		setTitle("Image View");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 413, 286);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnNewButton = new JButton("");
		btnNewButton.setIcon(new ImageIcon(FrmBai5_ImageView.class.getResource("/vn/t3h/resources/icon-Doraemon.jpg")));
		btnNewButton.setBounds(10, 11, 68, 69);
		contentPane.add(btnNewButton);
		
		JButton button = new JButton("");
		button.setIcon(new ImageIcon(FrmBai5_ImageView.class.getResource("/vn/t3h/resources/icon-Nobita.jpg")));
		button.setBounds(10, 91, 68, 69);
		contentPane.add(button);
		
		JButton button_1 = new JButton("");
		button_1.setIcon(new ImageIcon(FrmBai5_ImageView.class.getResource("/vn/t3h/resources/icon-Xuka.jpg")));
		button_1.setBounds(10, 171, 68, 69);
		contentPane.add(button_1);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(FrmBai5_ImageView.class.getResource("/vn/t3h/resources/Doraemon.jpg")));
		lblNewLabel.setBounds(88, 11, 300, 229);
		contentPane.add(lblNewLabel);
	}

}
