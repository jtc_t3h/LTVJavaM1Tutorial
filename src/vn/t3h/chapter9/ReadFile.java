package vn.t3h.chapter9;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class ReadFile {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		FileInputStream in = new FileInputStream("test.txt");
		
		byte [] b = new byte[2];
//		System.out.println((char)in.read(b));
		System.out.println(in.read(b)); // 2
		System.out.println(in.read(b)); // 
		for (byte e: b) {
			System.out.println((char)e); // ?
		}
		
		in.close();
	}

}
