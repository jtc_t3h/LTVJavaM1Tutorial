package vn.t3h.chapter9;

import java.io.Serializable;

public class Contact implements Serializable  {

	private String hoTen;
	private String dtdd;
	private String hinh;

	public Contact(String hoTen, String dtdd, String hinh) {
		this.hoTen = hoTen;
		this.dtdd = dtdd;
		this.hinh = hinh;
	}

	public String getHoTen() {
		return hoTen;
	}

	public void setHoTen(String hoTen) {
		this.hoTen = hoTen;
	}

	public String getDtdd() {
		return dtdd;
	}

	public void setDtdd(String dtdd) {
		this.dtdd = dtdd;
	}

	public String getHinh() {
		return hinh;
	}

	public void setHinh(String hinh) {
		this.hinh = hinh;
	}

}
