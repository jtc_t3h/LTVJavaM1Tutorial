package vn.t3h.chapter9;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;

import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.awt.event.ActionEvent;

public class FrmBai3_Contact extends JFrame {
	private JTextField txtHoTen;
	private JTextField txtDiDong;
	private JTextField txtHinhAnh;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai3_Contact frame = new FrmBai3_Contact();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai3_Contact() {
		setTitle("Th\u00EAm m\u1EDBi li\u00EAn h\u1EC7");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 506, 204);
		getContentPane().setLayout(null);

		JLabel label = new JLabel("H\u1ECD t\u00EAn");
		label.setBounds(21, 14, 73, 14);
		getContentPane().add(label);

		txtHoTen = new JTextField();
		txtHoTen.setColumns(10);
		txtHoTen.setBounds(104, 11, 252, 20);
		getContentPane().add(txtHoTen);

		JLabel label_1 = new JLabel("\u0110TD\u0110");
		label_1.setBounds(21, 42, 73, 14);
		getContentPane().add(label_1);

		txtDiDong = new JTextField();
		txtDiDong.setColumns(10);
		txtDiDong.setBounds(103, 39, 156, 20);
		getContentPane().add(txtDiDong);

		txtHinhAnh = new JTextField();
		txtHinhAnh.setColumns(10);
		txtHinhAnh.setBounds(103, 67, 193, 20);
		getContentPane().add(txtHinhAnh);

		JLabel label_2 = new JLabel("H\u00ECnh \u1EA3nh");
		label_2.setBounds(21, 70, 73, 14);
		getContentPane().add(label_2);

		JButton btnThemMoi = new JButton("Th\u00EAm m\u1EDBi");
		btnThemMoi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				File f = new File(txtHinhAnh.getText());
				String path = "src/vn/t3h/resources/";
				try {
					Files.copy(f.toPath(), (new File(path + f.getName())).toPath(),
							StandardCopyOption.REPLACE_EXISTING);
					System.out.println("Đã đưa hình liên hệ thư mục contact");
				} catch (IOException ex) {
					ex.printStackTrace();
					JOptionPane.showMessageDialog(null, "Lỗi! không tồn tại thư mục.");
				}

				String hoTen = txtHoTen.getText();
				String dtdd = txtDiDong.getText();
				String hinhAnh = f.getName();
				Contact h = new Contact(hoTen, dtdd, hinhAnh);

				ObjectOutputStream out = null;
				try {
//					out = new ObjectOutputStream(new FileOutputStream("src/vn/t3h/chapter9/contact.txt", true)) {
//						protected void writeStreamHeader() throws IOException {
//							reset();
//						}
//					};
					out = new ObjectOutputStream(new FileOutputStream("src/vn/t3h/chapter9/contact.txt", true));
					out.writeObject(h);
					
					JOptionPane.showMessageDialog(rootPane,	"Đã thêm liên hệ:" + h.getHoTen() + " - " + h.getDtdd() + " - " + h.getHinh());
				} catch (FileNotFoundException ex) {
					ex.printStackTrace();
					JOptionPane.showMessageDialog(null, "Lỗi! không tìm thấy file.");
				} catch (IOException ex) {
					ex.printStackTrace();
					JOptionPane.showMessageDialog(null, "Lỗi IO.");
				} finally {
					try {
						out.flush();
						out.close();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}

			}
		});
		btnThemMoi.setBounds(190, 121, 118, 23);
		getContentPane().add(btnThemMoi);

		JLabel lblHinhAnh = new JLabel("");
		lblHinhAnh.setBounds(366, 11, 118, 136);
		getContentPane().add(lblHinhAnh);

		JButton btnBrowser = new JButton("...");
		btnBrowser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser fcs = new JFileChooser();
				fcs.setFileSelectionMode(JFileChooser.FILES_ONLY);
				
				int retureVal = fcs.showOpenDialog(FrmBai3_Contact.this);
				
				if (retureVal == JFileChooser.APPROVE_OPTION) {
					File file = fcs.getSelectedFile();
					
					lblHinhAnh.setIcon(new ImageIcon(file.getPath()));
					txtHinhAnh.setText(file.getPath());
				}
			}
		});
		btnBrowser.setBounds(309, 66, 47, 23);
		getContentPane().add(btnBrowser);
	}

}
