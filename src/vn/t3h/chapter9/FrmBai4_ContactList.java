package vn.t3h.chapter9;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedInputStream;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class FrmBai4_ContactList extends JFrame {

	private JPanel contentPane;
	private JTextField txtName;
	private JTable table;

	private List<Contact> lst;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai4_ContactList frame = new FrmBai4_ContactList();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai4_ContactList() {
		setTitle("Tìm kiếm liên hệ");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblTn = new JLabel("Tên");
		lblTn.setBounds(24, 21, 46, 14);
		contentPane.add(lblTn);

		txtName = new JTextField();
		txtName.setBounds(80, 18, 184, 20);
		contentPane.add(txtName);
		txtName.setColumns(10);

		JButton btnSearch = new JButton("Tìm");
		btnSearch.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				DefaultTableModel model = (DefaultTableModel) table.getModel();
				
				boolean flag = false;
				for (Contact contact : lst) {
					if (contact.getHoTen().contains(txtName.getText())) {
						
						ImageIcon img = new ImageIcon("src/vn/t3h/resources/" + contact.getHinh());
						model.addRow(new Object[] { img, contact.getHoTen(), contact.getDtdd() });
						flag = true;
					}
				}
				if (!flag) {
					JOptionPane.showMessageDialog(rootPane, "Không tìm thấy liên hệ");
				} else {
					table.setModel(model);
					table.getColumn(table.getColumnName(0))
							.setCellRenderer(new ImageTableCellRenderer(60, 60));
				}
			}
		});
		btnSearch.setBounds(274, 17, 89, 23);
		contentPane.add(btnSearch);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 50, 414, 200);
		contentPane.add(scrollPane);

		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"H\u00ECnh \u1EA3nh", "H\u1ECD t\u00EAn", "\u0110TD\u0110"
			}
		));
		scrollPane.setViewportView(table);

		lst = new ArrayList<Contact>();  // 
		ObjectInputStream in = null;
		try {
			in = new ObjectInputStream(new BufferedInputStream(new FileInputStream("src/vn/t3h/chapter9/contact.txt")));
			while (true) {
				Contact contact = (Contact) in.readObject();
				System.out.println(contact);
				lst.add(contact);
			}
		} catch (EOFException ex) {
		} catch (ClassNotFoundException | IOException ex) {
			ex.printStackTrace();
			JOptionPane.showMessageDialog(null, "Lỗi đọc file.");
		} finally {
			try {
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
