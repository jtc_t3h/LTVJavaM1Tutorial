package vn.t3h.chapter9;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.awt.event.ActionEvent;

public class FrmBai1_Diary extends JFrame {

	private JPanel contentPane;
	private JTextField txtDateTime;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai1_Diary frame = new FrmBai1_Diary();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai1_Diary() {
		setTitle("Nhật ký hàng ngày");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNgyGi = new JLabel("Ngày - giờ");
		lblNgyGi.setBounds(158, 11, 71, 14);
		contentPane.add(lblNgyGi);

		txtDateTime = new JTextField();
		txtDateTime.setBounds(239, 8, 185, 20);
		contentPane.add(txtDateTime);
		txtDateTime.setColumns(10);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(77, 43, 347, 207);
		contentPane.add(scrollPane);

		JTextArea textArea = new JTextArea();
		scrollPane.setViewportView(textArea);

		JButton btnRead = new JButton("");
		btnRead.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String nhatKy = "";
				String ngay;
				String noiDung;

				DataInputStream in = null;
				try {
					in = new DataInputStream(
							new BufferedInputStream(new FileInputStream("src/vn/t3h/chapter9/diary.txt")));

					while (true) {
						ngay = in.readUTF();
						noiDung = in.readUTF();

						nhatKy += ngay + "\n" + noiDung + "\n\n";
					}
				} catch (EOFException ex) {
				} catch (IOException ex) {
					JOptionPane.showMessageDialog(null, "Lỗi! Không đọc được file!");
				} finally {
					try {
						in.close();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}

				textArea.setText(nhatKy);
				textArea.setEditable(false);
			}
		});
		btnRead.setIcon(new ImageIcon(FrmBai1_Diary.class.getResource("/vn/t3h/resources/read.png")));
		btnRead.setBounds(10, 45, 57, 47);
		contentPane.add(btnRead);

		JButton btnInsert = new JButton("");
		btnInsert.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
				Date date = new Date();
				txtDateTime.setText(dateFormat.format(date));
				textArea.setEditable(true);
				textArea.setText("");
			}
		});
		btnInsert.setIcon(new ImageIcon(FrmBai1_Diary.class.getResource("/vn/t3h/resources/edit.png")));
		btnInsert.setBounds(10, 123, 57, 47);
		contentPane.add(btnInsert);

		JButton btnSave = new JButton("");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String ngay = txtDateTime.getText();
				String noiDung = textArea.getText();

				DataOutputStream out = null;
				try {
					out = new DataOutputStream(
							new BufferedOutputStream(new FileOutputStream("src/vn/t3h/chapter9/diary.txt", true)));

					out.writeUTF(ngay);
					out.writeUTF(noiDung);

					JOptionPane.showMessageDialog(rootPane, "Nhật ký đã được ghi.");
				} catch (FileNotFoundException ex) {
					JOptionPane.showMessageDialog(null, "Lỗi! File không tồn tại.");
					ex.printStackTrace();
				} catch (IOException ex) {
					JOptionPane.showMessageDialog(null, "Lỗi! Không đọc được file.");
					ex.printStackTrace();
				} finally {
					if (out != null) {
						try {
							out.flush();
							out.close();
						} catch (IOException e1) {
							e1.printStackTrace();
						}

					}
				}
			}
		});
		btnSave.setIcon(new ImageIcon(FrmBai1_Diary.class.getResource("/vn/t3h/resources/save.jpg")));
		btnSave.setBounds(10, 203, 57, 47);
		contentPane.add(btnSave);

		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date date = new Date();
		txtDateTime.setText(dateFormat.format(date));
		textArea.setEditable(true);
		textArea.setText("");
	}

}
