package vn.t3h.chapter9;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class WriteFileDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		// byte stream
		try {
			FileOutputStream out = new FileOutputStream("test.txt");
			
			out.write(65);
			out.write(new byte[] {66, 67});
			
			out.flush();
			out.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			System.out.println("Done!");
		}
	}

}
