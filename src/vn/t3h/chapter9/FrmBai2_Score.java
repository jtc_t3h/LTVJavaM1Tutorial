package vn.t3h.chapter9;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionListener;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.awt.event.ActionEvent;

public class FrmBai2_Score extends JFrame {

	private JPanel contentPane;
	private JTextField txtHoTen;
	private JTextField txtTBHK1;
	private JTextField txtTBHK2;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai2_Score frame = new FrmBai2_Score();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai2_Score() {
		setTitle("Tổng kết năm học");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 411);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblHTn = new JLabel("Họ và tên");
		lblHTn.setBounds(10, 11, 82, 14);
		contentPane.add(lblHTn);

		JLabel lblKhiLp = new JLabel("Khối lớp");
		lblKhiLp.setBounds(10, 36, 82, 14);
		contentPane.add(lblKhiLp);

		JLabel lblimTbhk = new JLabel("Điểm TBHK1");
		lblimTbhk.setBounds(10, 61, 82, 14);
		contentPane.add(lblimTbhk);

		JLabel lblimTbhk_1 = new JLabel("Điểm TBHK2");
		lblimTbhk_1.setBounds(10, 86, 82, 14);
		contentPane.add(lblimTbhk_1);

		txtHoTen = new JTextField();
		txtHoTen.setBounds(102, 8, 120, 20);
		contentPane.add(txtHoTen);
		txtHoTen.setColumns(10);

		txtTBHK1 = new JTextField();
		txtTBHK1.setColumns(10);
		txtTBHK1.setBounds(102, 58, 120, 20);
		contentPane.add(txtTBHK1);

		txtTBHK2 = new JTextField();
		txtTBHK2.setColumns(10);
		txtTBHK2.setBounds(102, 83, 120, 20);
		contentPane.add(txtTBHK2);

		JComboBox cbbKhoi = new JComboBox();
		cbbKhoi.setModel(new DefaultComboBoxModel(new String[] { "10", "11", "12" }));
		cbbKhoi.setBounds(102, 33, 120, 20);
		contentPane.add(cbbKhoi);

		JButton btnThemMoi = new JButton("Thêm mới");
		btnThemMoi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String hoTen = txtHoTen.getText();
				int khoi = Integer.parseInt(cbbKhoi.getSelectedItem().toString());
				double tbhk1 = Double.parseDouble(txtTBHK1.getText());
				double tbhk2 = Double.parseDouble(txtTBHK2.getText());

				DataOutputStream out = null;
				try {
					out = new DataOutputStream(
							new BufferedOutputStream(new FileOutputStream("src/vn/t3h/chapter9/score.txt", true)));
					out.writeUTF(hoTen);
					out.writeInt(khoi);
					out.writeDouble(tbhk1);
					out.writeDouble(tbhk2);
					JOptionPane.showMessageDialog(rootPane, "Kết quả của học sinh đã được ghi.");

				} catch (FileNotFoundException ex) {
					ex.printStackTrace();
					JOptionPane.showMessageDialog(null, "Lỗi! Không tìm thấy file.");
				} catch (IOException ex) {
					ex.printStackTrace();
					JOptionPane.showMessageDialog(null, "Lỗi IO.");
				} finally {
					try {
						out.flush();
						out.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}

			}
		});
		btnThemMoi.setBounds(232, 7, 120, 23);
		contentPane.add(btnThemMoi);

		JButton btnTiepTuc = new JButton("Tiếp tục");
		btnTiepTuc.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				txtHoTen.setText("");
				cbbKhoi.setSelectedIndex(0);
				txtTBHK1.setText("");
				txtTBHK2.setText("");
			}
		});
		btnTiepTuc.setBounds(232, 32, 120, 23);
		contentPane.add(btnTiepTuc);

		JButton btnDocDS = new JButton("Đọc danh sách");
		btnDocDS.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String hoten = "";
				double tbhk1 = 0d;
				double tbhk2 = 0d;
				double tbcn = 0d;
				int khoi = 0;

				DefaultTableModel model = (DefaultTableModel) table.getModel();

				DataInputStream in = null;
				try {
					in = new DataInputStream(
							new BufferedInputStream(new FileInputStream("src/vn/t3h/chapter9/score.txt")));

					while (true) {
						hoten = in.readUTF();
						khoi = in.readInt();
						tbhk1 = in.readDouble();
						tbhk2 = in.readDouble();
						tbcn = (tbhk1 + tbhk2 * 2) / 3;

						DecimalFormat df = new DecimalFormat("###.##");
						tbcn = Double.parseDouble(df.format(tbcn));
						model.addRow(new Object[] { hoten, khoi, tbhk1, tbhk2, tbcn });
					}
				} catch (EOFException ex) {

				} catch (FileNotFoundException ex) {
					ex.printStackTrace();
					JOptionPane.showMessageDialog(null, "Lỗi! không tồn tại file.");
				} catch (IOException ex) {
					ex.printStackTrace();
					JOptionPane.showMessageDialog(null, "Lỗi IO.");
				} finally {
					try {
						in.close();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}

				table.setModel(model);

			}
		});
		btnDocDS.setBounds(232, 57, 120, 23);
		contentPane.add(btnDocDS);

		JButton btnThongKe = new JButton("Thống kê");
		btnThongKe.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				String hoten = "";
				double tbhk1 = 0d;
				double tbhk2 = 0d;
				double tbcn = 0d;
				int khoi = 0;

				int soHSGioi = 0;
				int soHSKha = 0;
				int soHSTBKhaTB = 0;
				int soHSYeuKem = 0;

				DataInputStream in = null;
				try {
					in = new DataInputStream(
							new BufferedInputStream(new FileInputStream("src/vn/t3h/chapter9/score.txt")));

					while (true) {
						hoten = in.readUTF();
						khoi = in.readInt();
						tbhk1 = in.readDouble();
						tbhk2 = in.readDouble();
						tbcn = (tbhk1 + tbhk2 * 2) / 3;

						if (tbcn >= 8) {
							soHSGioi++;
						} else if (tbcn >= 7 && tbcn < 8) {
							soHSKha++;
						} else if (tbcn >= 5 && tbcn < 7) {
							soHSTBKhaTB++;
						} else {
							soHSYeuKem++;
						}
					}
				} catch (EOFException ex) {

				} catch (FileNotFoundException ex) {
					ex.printStackTrace();
					JOptionPane.showMessageDialog(null, "Lỗi! không tồn tại file.");
				} catch (IOException ex) {
					ex.printStackTrace();
					JOptionPane.showMessageDialog(null, "Lỗi IO.");
				} finally {
					try {
						in.close();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}

				String ketQua = "Số HS giỏi: " + soHSGioi + "\n Số HS khá: " + soHSKha + "\n Số HS TB khá & TB: "
						+ soHSTBKhaTB + "\n Số HS yếu & kém: " + soHSYeuKem;

				JOptionPane.showMessageDialog(rootPane, ketQua, "Thống kê", 1);
			}
		});
		btnThongKe.setBounds(232, 82, 120, 23);
		contentPane.add(btnThongKe);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 111, 414, 250);
		contentPane.add(scrollPane);

		table = new JTable();
		table.setModel(new DefaultTableModel(new Object[][] {}, new String[] { "H\u1ECD v\u00E0 t\u00EAn", "Kh\u1ED1i",
				"\u0110i\u1EC3m TBHK1", "\u0110i\u1EC3m TBHK2", "TB c\u1EA3 n\u0103m" }));
		scrollPane.setViewportView(table);
	}
}
