package vn.t3h.chapter8;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class FrmBai2_Gift extends JFrame {

	private JPanel contentPane;
	private JTextField txtHinhDang;
	private JTextField txtMauSac;
	private JTextField txtTenQua;
	private JTextField txtTrongLuong;
	private JTextArea textArea;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai2_Gift frame = new FrmBai2_Gift();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai2_Gift() {
		setTitle("Dịch vụ gửi quà");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 435);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblThngTinHp = new JLabel("Thông tin hộp quà");
		lblThngTinHp.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblThngTinHp.setBounds(10, 11, 414, 14);
		contentPane.add(lblThngTinHp);

		JLabel lblHnhDngHp = new JLabel("Hình dáng hộp quà");
		lblHnhDngHp.setBounds(10, 36, 109, 14);
		contentPane.add(lblHnhDngHp);

		txtHinhDang = new JTextField();
		txtHinhDang.setBounds(130, 33, 294, 20);
		contentPane.add(txtHinhDang);
		txtHinhDang.setColumns(10);

		txtMauSac = new JTextField();
		txtMauSac.setColumns(10);
		txtMauSac.setBounds(130, 61, 294, 20);
		contentPane.add(txtMauSac);

		JLabel lblMuSc = new JLabel("Màu sắc");
		lblMuSc.setBounds(10, 64, 109, 14);
		contentPane.add(lblMuSc);

		JLabel lblThngTinQu = new JLabel("Thông tin quà");
		lblThngTinQu.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblThngTinQu.setBounds(10, 102, 414, 14);
		contentPane.add(lblThngTinQu);

		JLabel lblTnQu = new JLabel("Tên quà");
		lblTnQu.setBounds(10, 127, 109, 14);
		contentPane.add(lblTnQu);

		txtTenQua = new JTextField();
		txtTenQua.setBounds(131, 124, 293, 20);
		contentPane.add(txtTenQua);
		txtTenQua.setColumns(10);

		txtTrongLuong = new JTextField();
		txtTrongLuong.setColumns(10);
		txtTrongLuong.setBounds(130, 152, 226, 20);
		contentPane.add(txtTrongLuong);

		JLabel lblTrngLng = new JLabel("Trọng lượng");
		lblTrngLng.setBounds(10, 155, 109, 14);
		contentPane.add(lblTrngLng);

		JLabel lblG = new JLabel("g");
		lblG.setBounds(366, 155, 58, 14);
		contentPane.add(lblG);

		JButton btnGuiQua = new JButton("Gửi quà");
		btnGuiQua.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				Bai2_GiftBox gb = new Bai2_GiftBox(txtHinhDang.getText(), txtMauSac.getText());
				Bai2_GiftBox.Gift g = gb.new Gift(txtTenQua.getText(), Double.parseDouble(txtTrongLuong.getText()));

				String thongTin = "Hộp quà: Có hình:" + gb.getHinhDang() +
						"\nvà giấy bao màu: " + gb.getMauSac() +
						",\nphí làm hộp " + String.valueOf(gb.getPhiLamHop()) +
						"\nBên trong đựng quà: \n" + g.getTen() + 
						"\n có trọng lượng " + String.valueOf(g.getTrongLuong()) + "g\n" + 
						"Tiền gửi quà: " + String.valueOf(g.tinhTienGui());
				
				textArea.setText(thongTin);
			}
		});
		btnGuiQua.setBounds(130, 183, 129, 23);
		contentPane.add(btnGuiQua);

		JLabel lblThngTinGi = new JLabel("Thông tin gửi quà");
		lblThngTinGi.setBounds(10, 224, 109, 14);
		contentPane.add(lblThngTinGi);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(130, 224, 294, 161);
		contentPane.add(scrollPane);

		textArea = new JTextArea();
		scrollPane.setViewportView(textArea);
	}
}
