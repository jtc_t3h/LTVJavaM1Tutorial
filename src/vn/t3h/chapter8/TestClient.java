package vn.t3h.chapter8;

public class TestClient {

	public static void main(String[] args) {
		MyOuter myOuter = new MyOuter();
		
		//MyOuter.MyInner myInner = new MyOuter().new MyInner();
		MyOuter.MyInner myInner = myOuter.new MyInner();
	}

}
