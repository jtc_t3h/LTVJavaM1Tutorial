package vn.t3h.chapter8;

public class MyOuter {

	/* Modifier */ class MyInner {
		public void display() {
			System.out.println("Hello! Regular inner class.");
		}
	}
}
