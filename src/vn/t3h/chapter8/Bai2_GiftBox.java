package vn.t3h.chapter8;

public class Bai2_GiftBox {

	private String hinhDang;
	private String mauSac;

	private final int phiLamHop = 10000;

	public Bai2_GiftBox(String hinhDang, String mauSac) {
		this.hinhDang = hinhDang;
		this.mauSac = mauSac;
	}

	public class Gift {
		private String ten;
		private Double trongLuong;

		private final int donGia = 200;

		public Gift(String ten, Double trongLuong) {
			this.ten = ten;
			this.trongLuong = trongLuong;
		}

		public Double tinhTienGui() {
			return trongLuong * donGia + phiLamHop;
		}

		public String getTen() {
			return ten;
		}

		public void setTen(String ten) {
			this.ten = ten;
		}

		public int getDonGia() {
			return donGia;
		}

		public Double getTrongLuong() {
			return trongLuong;
		}

		public void setTrongLuong(Double trongLuong) {
			this.trongLuong = trongLuong;
		}

	}

	public String getHinhDang() {
		return hinhDang;
	}

	public void setHinhDang(String hinhDang) {
		this.hinhDang = hinhDang;
	}

	public String getMauSac() {
		return mauSac;
	}

	public void setMauSac(String mauSac) {
		this.mauSac = mauSac;
	}

	public int getPhiLamHop() {
		return phiLamHop;
	}

}
