package vn.t3h.chapter4;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.ListModel;
import javax.swing.JScrollPane;
import javax.swing.JList;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.awt.event.ActionEvent;
import javax.swing.AbstractListModel;

public class FrmBai5_ScoreBoard extends JFrame {

	private JPanel contentPane;
	private JTextField txtName;
	private JTextField txtScore;
	private JTextField txtKeyword;
	private JList lstMain;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai5_ScoreBoard frame = new FrmBai5_ScoreBoard();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai5_ScoreBoard() {
		setTitle("Score Board");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 470, 277);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblName = new JLabel("Name");
		lblName.setBounds(10, 11, 46, 14);
		contentPane.add(lblName);
		
		txtName = new JTextField();
		txtName.setBounds(70, 8, 103, 20);
		contentPane.add(txtName);
		txtName.setColumns(10);
		
		JLabel lblScore = new JLabel("Score");
		lblScore.setBounds(199, 11, 46, 14);
		contentPane.add(lblScore);
		
		txtScore = new JTextField();
		txtScore.setBounds(239, 8, 86, 20);
		contentPane.add(txtScore);
		txtScore.setColumns(10);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 36, 315, 190);
		contentPane.add(scrollPane);
		
		lstMain = new JList();
		lstMain.setModel(new AbstractListModel() {
			String[] values = new String[] {};
			public int getSize() {
				return values.length;
			}
			public Object getElementAt(int index) {
				return values[index];
			}
		});
		scrollPane.setViewportView(lstMain);
		
		JButton btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String name = txtName.getText().trim();
				int score = Integer.parseInt(txtScore.getText().trim());
				
				ListModel<String> model = lstMain.getModel();
				if (model == null || model.getSize() == 0) {
					model = new DefaultListModel<String>();
				}
				((DefaultListModel)model).addElement(name + " " + score);
				lstMain.setModel(model);
			}
		});
		btnAdd.setBounds(335, 7, 109, 39);
		contentPane.add(btnAdd);
		
		JButton btnSortName = new JButton("Sort Name");
		btnSortName.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String[] array = getArray();
				Arrays.sort(array, new Bai5_NameComparator());
				
				ListModel<String> model = arrayToListModel(array);
				lstMain.setModel(model);
			}
		});
		btnSortName.setBounds(335, 57, 109, 39);
		contentPane.add(btnSortName);
		
		JButton btnSortScore = new JButton("Sort Score");
		btnSortScore.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String[] array = getArray();
				Arrays.sort(array, new Bai5_ScoreComparator());
				
				ListModel<String> model = arrayToListModel(array);
				lstMain.setModel(model);
			}
		});
		btnSortScore.setBounds(335, 107, 109, 39);
		contentPane.add(btnSortScore);
		
		txtKeyword = new JTextField();
		txtKeyword.setBounds(335, 157, 109, 20);
		contentPane.add(txtKeyword);
		txtKeyword.setColumns(10);
		
		JButton btnFind = new JButton("Find");
		btnFind.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String keyword = txtKeyword.getText().trim();
				
				String[] array = search(keyword);
				
				ListModel<String> model = arrayToListModel(array);
				lstMain.setModel(model);
				
			}
		});
		btnFind.setBounds(335, 188, 109, 39);
		contentPane.add(btnFind);
	}
	
	protected String[] search(String keyword) {
		String[]arr = null;
		int idx2 = 0;
		
		ListModel<String> model = lstMain.getModel();
		if (model != null ) {
			arr = new String[model.getSize()];
			
			
			for (int idx = 0; idx < model.getSize(); idx++) {
				if (model.getElementAt(idx).contains(keyword)){
					arr[idx2++] = model.getElementAt(idx);
				}
			}
		}
		
		return Arrays.copyOfRange(arr, 0, idx2);
	}

	protected ListModel<String> arrayToListModel(String[] array) {
		DefaultListModel<String> model = new DefaultListModel<String>();
		
		for(String item: array) {
			model.addElement(item);
		}
		
		return model;
	}

	protected String[] getArray() {
		String[]arr = null;
		
		ListModel<String> model = lstMain.getModel();
		if (model != null ) {
			arr = new String[model.getSize()];
			
			for (int idx = 0; idx < model.getSize(); idx++) {
				arr[idx] = model.getElementAt(idx);
			}
		} 
		
		return arr;
	}

}
