package vn.t3h.chapter4;

import java.util.Comparator;

public class Bai5_NameComparator implements Comparator<String> {

	@Override
	public int compare(String item1, String item2) {
		return item1.split(" ")[0].compareTo(item2.split(" ")[0]);
	}

}
