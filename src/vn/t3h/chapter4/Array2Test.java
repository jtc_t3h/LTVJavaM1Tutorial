package vn.t3h.chapter4;

public class Array2Test {
	public static void main(String[] args) {
		int [][] arr = {{1,2,3},{4,5,6}};
		
		for (int[] arrRow: arr) {
			for (int element: arrRow) {
				System.out.println(element);
			}
		}
	}
}
