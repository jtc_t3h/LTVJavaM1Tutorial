package vn.t3h.chapter4;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.Random;
import java.awt.event.ActionEvent;

public class FrmBai1_TinhTong extends JFrame {

	private JPanel contentPane;
	private JTextField txtN;
	private JTextField txtTong;
	private JTextArea taMang;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai1_TinhTong frame = new FrmBai1_TinhTong();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai1_TinhTong() {
		setTitle("T\u00EDnh t\u1ED5ng c\u00E1c ph\u1EA7n t\u1EED c\u00F3 gi\u00E1 tr\u1ECB ng\u1EABu nhi\u00EAn trong m\u1EA3ng");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNhpN = new JLabel("Nh\u1EADp n");
		lblNhpN.setBounds(10, 11, 46, 14);
		contentPane.add(lblNhpN);
		
		txtN = new JTextField();
		txtN.setBounds(103, 8, 86, 20);
		contentPane.add(txtN);
		txtN.setColumns(10);
		
		JLabel lblMngPhtSinh = new JLabel("M\u1EA3ng ph\u00E1t sinh");
		lblMngPhtSinh.setBounds(10, 46, 86, 14);
		contentPane.add(lblMngPhtSinh);
		
		taMang = new JTextArea();
		taMang.setLineWrap(true);
		taMang.setBounds(103, 41, 321, 123);
		contentPane.add(taMang);
		
		JLabel lblTng = new JLabel("T\u1ED5ng");
		lblTng.setBounds(10, 178, 46, 14);
		contentPane.add(lblTng);
		
		txtTong = new JTextField();
		txtTong.setBounds(103, 175, 86, 20);
		contentPane.add(txtTong);
		txtTong.setColumns(10);
		
		JButton btnTinhTong = new JButton("T\u00EDnh t\u1ED5ng");
		btnTinhTong.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// Lấy số N từ người dùng nhập vào
				String strN = txtN.getText().trim();
				int n = Integer.parseInt(strN);
				
				// Gọi hàm để lấy danh sách số nguyên phát sinh ngẫu nhiễn
				int [] arr = randomArray(n);
				
				// Hiển thị mảng trong TextArea
				String strMang = arrayToString(arr);
				taMang.setText(strMang);
				
				int tong = sum(arr);
				txtTong.setText(String.valueOf(tong));
			}
		});
		btnTinhTong.setBounds(100, 214, 89, 23);
		contentPane.add(btnTinhTong);
	}

	protected int sum(int[] arr) {
		int tong = 0;
		
		for (int element: arr) {
			tong += element;
		}
		
		return tong;
	}

	protected String arrayToString(int[] arr) {
		String strMang = "";
		
		for (int element: arr) {
			strMang += element + " ";
		}
		
		return strMang;
	}

	/**
	 * Sinh ngẫu nhiên danh sách n số nguyên nhỏ hơn 100
	 * @param n
	 * 		số lượng sinh ngẫu nhiên
	 * @return
	 * 	Mảng số nguyên nhỏ hơn 100
	 */
	protected int[] randomArray(int n) {
		int [] arr = new int[n];
		
		Random rd = new Random();
		for (int idx = 0; idx < n; idx++) {
			arr[idx] = rd.nextInt(100);
		}
		
		return arr;
	}
}
