package vn.t3h.chapter4;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.Random;
import java.awt.event.ActionEvent;

public class FrmBai2_Mang1Chieu extends JFrame {

	private JPanel contentPane;
	private JTextField txtN;
	private JTextField txtX;
	private JTextArea taMang;
	private JTextArea taKetQua;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai2_Mang1Chieu frame = new FrmBai2_Mang1Chieu();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai2_Mang1Chieu() {
		setTitle("C\u00E1c thao t\u00E1c tr\u00EAn m\u1EA3ng 1 chi\u1EC1u\r\n");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 380, 328);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNhpN = new JLabel("Nh\u1EADp n");
		lblNhpN.setBounds(10, 11, 46, 14);
		contentPane.add(lblNhpN);
		
		txtN = new JTextField();
		txtN.setBounds(111, 8, 86, 20);
		contentPane.add(txtN);
		txtN.setColumns(10);
		
		JLabel lblNhpX = new JLabel("Nh\u1EADp x");
		lblNhpX.setBounds(10, 41, 46, 14);
		contentPane.add(lblNhpX);
		
		txtX = new JTextField();
		txtX.setBounds(111, 39, 86, 20);
		contentPane.add(txtX);
		txtX.setColumns(10);
		
		JLabel lblMngcPht = new JLabel("M\u1EA3ng ph\u00E1t sinh");
		lblMngcPht.setBounds(10, 74, 91, 14);
		contentPane.add(lblMngcPht);
		
		taMang = new JTextArea();
		taMang.setLineWrap(true);
		taMang.setBounds(111, 69, 243, 83);
		contentPane.add(taMang);
		
		JLabel lblKtQuTm = new JLabel("K\u1EBFt qu\u1EA3 t\u00ECm ki\u1EBFm");
		lblKtQuTm.setBounds(10, 163, 102, 14);
		contentPane.add(lblKtQuTm);
		
		taKetQua = new JTextArea();
		taKetQua.setLineWrap(true);
		taKetQua.setBounds(111, 163, 243, 71);
		contentPane.add(taKetQua);
		
		JButton btnTimKiem = new JButton("T\u00ECm ki\u1EBFm");
		btnTimKiem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String strN = txtN.getText().trim();
				String strX = txtX.getText().trim();
				int n = Integer.parseInt(strN);
				int x = Integer.parseInt(strX);
				
				int[] array = randomArray(n);
				
				String strArr = arrayToString(array);
				taMang.setText(strArr);
				
				String strKetQua = "";
				
				int indexOfArr = searchX(array, x);
				if (indexOfArr == -1) {
					strKetQua += "Không tìm thấy \n";
				} else {
					strKetQua += "Tìm thấy tại vị trí " + indexOfArr + "\n";
				}
				
				boolean isGreater = isGreater(array, x);
				if (isGreater) {
					strKetQua += "X lớn hơn tất cả \n";
				} else {
					strKetQua += "X không lớn tất cả \n";
				}
				
				String strLess = lessOfArrayByX(array, x);
				if (!strLess.isEmpty()) {
					strKetQua += "X nhỏ hơn: " + strLess;
				}
				
				taKetQua.setText(strKetQua);
				
			}
		});
		btnTimKiem.setBounds(98, 255, 89, 23);
		contentPane.add(btnTimKiem);
	}

	protected String lessOfArrayByX(int[] array, int x) {
		String strLess = "";
		
		for (int element: array) {
			if (x < element) {
				strLess += element + " ";
			}
		}
		
		return strLess;
	}

	protected boolean isGreater(int[] array, int x) {
		
		for (int element: array) {
			if (x <= element) {
				return false;
			}
		}
		
		return true;
	}

	protected int searchX(int[] array, int x) {
		
		for (int idx = 0; idx < array.length; idx++) {
			if (array[idx] == x) {
				return idx;
			}
		}
		return -1;
	}

	protected String arrayToString(int[] array) {
		String strArr = "";
		
		for (int element: array) {
			strArr += element + " ";
		}
		
		return strArr;
	}

	protected int[] randomArray(int n) {
		Random rd = new Random();
		int []array = new int[n];
		
		for (int idx = 0; idx < n; idx++) {
			array[idx] = rd.nextInt(100);
		}
		
		return array;
	}

}
