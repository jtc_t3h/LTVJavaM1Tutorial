package vn.t3h.chapter4;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.ListModel;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JList;
import javax.swing.AbstractListModel;
import javax.swing.DefaultListModel;

import java.awt.event.ActionListener;
import java.util.Arrays;
import java.awt.event.ActionEvent;

public class FrmBai4_SortByAge extends JFrame {

	private JPanel contentPane;
	private JTextField txtName;
	private JTextField txtBirth;
	private JList lstMain;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai4_SortByAge frame = new FrmBai4_SortByAge();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai4_SortByAge() {
		setTitle("Sort by age");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 334, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblName = new JLabel("Name");
		lblName.setBounds(10, 11, 46, 14);
		contentPane.add(lblName);
		
		txtName = new JTextField();
		txtName.setBounds(68, 8, 141, 20);
		contentPane.add(txtName);
		txtName.setColumns(10);
		
		JLabel Birthday = new JLabel("Birth");
		Birthday.setBounds(10, 39, 46, 14);
		contentPane.add(Birthday);
		
		txtBirth = new JTextField();
		txtBirth.setColumns(10);
		txtBirth.setBounds(68, 36, 141, 20);
		contentPane.add(txtBirth);
		
		JButton btnNewButton = new JButton("Add");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				String name = txtName.getText().trim();
				String birth = txtBirth.getText().trim();
				String item = name + " " + birth;
				
				String[] array = getArray(item);
				Arrays.sort(array, new Bai4_AgeComparator());
				
				ListModel<String> model = arrayToListModel(array);
				lstMain.setModel(model);
			}
		});
		btnNewButton.setBounds(219, 7, 89, 46);
		contentPane.add(btnNewButton);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 66, 298, 184);
		contentPane.add(scrollPane);
		
		lstMain = new JList();
		scrollPane.setViewportView(lstMain);
		
	}

	protected ListModel<String> arrayToListModel(String[] array) {
		DefaultListModel<String> model = new DefaultListModel<String>();
		
		for(String item: array) {
			model.addElement(item);
		}
		
		return model;
	}

	protected String[] getArray(String item) {
		String[]arr = null;
		
		ListModel<String> model = lstMain.getModel();
		if (model != null) {
			arr = new String[model.getSize() + 1];
			for (int idx = 0; idx < model.getSize(); idx++) {
				arr[idx] = model.getElementAt(idx);
			}
		} else {
			arr = new String[1];
		}
		arr[arr.length - 1] = item;
		
		return arr;
	}

}
