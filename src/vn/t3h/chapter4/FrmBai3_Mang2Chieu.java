package vn.t3h.chapter4;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import javax.swing.JList;
import java.awt.event.ActionListener;
import java.util.Random;
import java.awt.event.ActionEvent;

public class FrmBai3_Mang2Chieu extends JFrame {

	private JPanel contentPane;
	private JTextField txtN;
	private JTextField txtM;
	private JTextArea taRandom;
	private JTextArea taKetQua;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai3_Mang2Chieu frame = new FrmBai3_Mang2Chieu();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai3_Mang2Chieu() {
		setTitle("X\u1EED l\u00FD m\u1EA3ng 2 chi\u1EC1u");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 432, 395);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNhpNdng = new JLabel("Nh\u1EADp n (d\u00F2ng)");
		lblNhpNdng.setBounds(10, 11, 91, 14);
		contentPane.add(lblNhpNdng);
		
		txtN = new JTextField();
		txtN.setBounds(135, 11, 100, 20);
		contentPane.add(txtN);
		txtN.setColumns(10);
		
		JLabel lblNhpMct = new JLabel("Nh\u1EADp m (c\u1ED9t)");
		lblNhpMct.setBounds(10, 36, 91, 14);
		contentPane.add(lblNhpMct);
		
		txtM = new JTextField();
		txtM.setColumns(10);
		txtM.setBounds(135, 36, 100, 20);
		contentPane.add(txtM);
		
		JButton btnRandomAndProcess = new JButton("Ph\u00E1t sinh v\u00E0 x\u1EED l\u00FD");
		btnRandomAndProcess.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				String strN = txtN.getText().trim();
				String strM = txtM.getText().trim();
				
				int n = Integer.parseInt(strN);
				int m = Integer.parseInt(strM);
				
				int[][] array = generateArray(n, m);
				
				String strArray = arrayToString(array);
				taRandom.setText(strArray);
				
				String strXuLy = processArray(array);
				taKetQua.setText(strXuLy);
			}
		});
		btnRandomAndProcess.setBounds(135, 70, 140, 23);
		contentPane.add(btnRandomAndProcess);
		
		JLabel lblMngcPht = new JLabel("M\u1EA3ng \u0111\u01B0\u1EE3c ph\u00E1t sinh");
		lblMngcPht.setBounds(10, 107, 111, 14);
		contentPane.add(lblMngcPht);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(135, 104, 269, 99);
		contentPane.add(scrollPane);
		
		taRandom = new JTextArea();
		scrollPane.setViewportView(taRandom);
		
		JLabel lblKtQuX = new JLabel("K\u1EBFt qu\u1EA3 x\u1EED l\u00FD");
		lblKtQuX.setBounds(10, 220, 111, 14);
		contentPane.add(lblKtQuX);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(137, 220, 267, 125);
		contentPane.add(scrollPane_1);
		
		taKetQua = new JTextArea();
		scrollPane_1.setViewportView(taKetQua);
	}

	protected String processArray(int[][] array) {
		
		int numberOfEven = 0;
		int numberOfOdd = 0;
		int sumOfEven = 0;
		int sumOfOdd = 0;
		int max = array[0][0];
		int min = array[0][0];
		
		for (int [] arrayN: array) {
			for (int element: arrayN) {
				if (element % 2 == 0) {
					numberOfEven += 1;
					sumOfEven += element;
				} else {
					numberOfOdd += 1;
					sumOfOdd += element;
				}
				if (max < element) {
					max = element;
				}
				if (min > element) {
					min = element;
				}
			}
			
		}
		
		String strXuLy = "";
		strXuLy += "Số phần tử chẵn = " + numberOfEven + "\n";
		strXuLy += "Số phần tử lẻ = " + numberOfOdd + "\n";
		strXuLy += "Trung bình chẵn = " + String.format("%.2f", (float)(sumOfEven/numberOfEven)) + "\n";
		strXuLy += "Trung bình lẻ = " + String.format("%.2f", (float)(sumOfOdd/numberOfEven)) + "\n";
		strXuLy += "GT max = " + max + "\n";
		strXuLy += "GT min = " + min + "\n";
		return strXuLy;
	}

	protected String arrayToString(int[][] array) {
		String strArray = "";
		
		for (int [] arrayN: array) {
			for (int element: arrayN) {
				strArray += element + " ";
			}
			strArray += "\n";
		}
		
		return strArray;
	}

	protected int[][] generateArray(int n, int m) {
		Random rd = new Random();
		
		int [][]array = new int[n][m];
		for (int idxN = 0; idxN < n; idxN++) {
			for (int idxM = 0; idxM < m; idxM++) {
				array[idxN][idxM] = rd.nextInt(100);
			}
		}
		
		return array;
	}
}
