package vn.t3h.chapter4;

import java.util.Arrays;
import java.util.Random;

public class ArrayTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
//		int [] arr = new int[5];
		
		// Duyet va truy xuat gia cua tung phan tu
//		for (int element: arr) {
//			System.out.println(element);
//		}
		
//		// Duyet cac phan tu cua mang
//		for (int idx = 1; idx <= arr.length; idx++) {
//			System.out.println(arr[idx]);
//		}
//		
		int n = 20;
		int arr[] = new int[n];
 		
		// Tao ra so ngau nhien 
		// C1: Su dung lop Math (java.lang)
		int idx = 0;
//		while (idx < 20) {
//			arr[idx++] = (int)(Math.random() * 100);
//		}
		// C2: su dung lop Random (java.util)
		Random rd = new Random();
		int tong = 0;
		while (idx < 20) {
			int temp = rd.nextInt();
			if (temp < 100 && temp > -100) {
				arr[idx++] = temp;
				tong += temp;
			}
		}
		
		for (int element: arr) {
			System.out.println(element);
		}
		System.out.println("Tong cac phan tu ngau nhien = " + tong);
		
		// Sap xep cac phan tu cua mang
//		Arrays.sort(arr);
//		
//		System.out.println("Ket qua sau sap xep");
//		for (int element: arr) {
//			System.out.println(element);
//		}
		
		System.out.println(Arrays.binarySearch(arr, 4));
	}

}
