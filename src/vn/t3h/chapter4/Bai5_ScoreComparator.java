package vn.t3h.chapter4;

import java.util.Comparator;

public class Bai5_ScoreComparator implements Comparator<String> {

	@Override
	public int compare(String item1, String item2) {
		int score1 = Integer.parseInt(item1.split(" ")[1]);
		int score2 = Integer.parseInt(item2.split(" ")[1]);
		return score1 - score2;
	}

}
